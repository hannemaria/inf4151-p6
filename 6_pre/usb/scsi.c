#include "../util.h"
#include "../thread.h"
#include "scsi.h"
#include "allocator.h"
#include "debug.h"
#include "error.h"

DEBUG_NAME("SCSI");

/* 
 * For purpose of this OS we assume there is only 
 * one SCSI device which is a USB flash drive 
 */
static struct scsi_dev *scsi = NULL;
static int scsi_dev_lock;

static int scsi_read_write(int dir, int block_start,
                           int block_count, char *data);

static int scsi_read_capacity(void);
static int scsi_test_unit_ready(void);

void scsi_static_init(void) {
  spinlock_init(&scsi_dev_lock);
}

int scsi_init(struct scsi_ifc *ifc) {
  int attempts;
  int rc;

  spinlock_acquire(&scsi_dev_lock);
  if (scsi != NULL) {
    /* A second USB disk has been inserted -> reject it */
    spinlock_release(&scsi_dev_lock);
    return ERR_PROTO;
  }

  scsi = kzalloc(sizeof(struct scsi_dev));
  if (scsi == NULL) {
    spinlock_release(&scsi_dev_lock);
    return ERR_NO_MEM;
  }

  /* Copy the driver interface */
  scsi->driver = ifc->driver;
  scsi->read = ifc->read;
  scsi->write = ifc->write;
  scsi->op_status = SCSI_OFF;

  rc = scsi_test_unit_ready();

  attempts = 20;

  while ((rc < 0) && (attempts-- > 0)) {
    ms_delay(500);
    rc = scsi_test_unit_ready();
  };

  if (rc < 0) {
    scsi_free(scsi);
    spinlock_release(&scsi_dev_lock);
    return ERR_PROTO;
  }

  rc = scsi_read_capacity();
  DEBUG("Reading device capacity parameters %s", DEBUG_STATUS(rc));

  if(rc < 0) {
    spinlock_release(&scsi_dev_lock);
    scsi_free(scsi);
    return ERR_PROTO;
  }

  DEBUG("Device block size %d, block count %d", 
      scsi->block_size, scsi->total_block_count);

  scsi->op_status = SCSI_ON;
  spinlock_release(&scsi_dev_lock);

  return 0;
}

/* Frees the SCSI device, releasing resources */
void scsi_free() {
  /* If any operation is in progress wait to finish */
  spinlock_acquire(&scsi_dev_lock);

  /* Device cannot accept more transfers */
  scsi->op_status = SCSI_OFF;

  /* We can safely release resources and clear the pointer */
  if (scsi != NULL)
    kfree(scsi);

  scsi = NULL;
  spinlock_release(&scsi_dev_lock);
}

static int _scsi_up() {
  int rc;

  if (scsi == NULL) {
    rc = SCSI_OFF;
  } else {
    rc = scsi->op_status;
  }

  return rc;
}

int scsi_up() {
  int rc;

  spinlock_acquire(&scsi_dev_lock);
  rc = _scsi_up();
  spinlock_release(&scsi_dev_lock);

  return rc;
}

static int scsi_test_unit_ready(void) {
  struct command_descriptor_block6 cdb6;
  int rc;


  DEBUG("Test unit ready");

  bzero((char *)&cdb6, sizeof(cdb6));
  cdb6.op_code = CDB_TEST_UNIT_READY;
 
  rc = scsi->read(scsi->driver, sizeof(cdb6), (char *)&cdb6, 0, NULL);

//  if (rc == SCSI_RC_FAILED) {
  if (rc < 0) {
    DEBUG("Device is not ready");
    return -1;
  }

  return 0;
}
/* 
 * Read driver parameters 
 */
struct capacity_data {
  uint32_t total_block_count;
  uint32_t block_size;
} __attribute__((packed));

static int scsi_read_capacity(void) {
  struct command_descriptor_block10 cdb10;
  struct capacity_data cap_data;
  int cap_data_size = sizeof(struct capacity_data);
  int rc;

  DEBUG("Read capacity");

  bzero((char *)&cdb10, sizeof(cdb10));
  cdb10.op_code = CDB_READ_CAPACITY10;

  rc = scsi->read(scsi->driver, sizeof(cdb10), (char *)&cdb10, 
                                cap_data_size, (char *)&cap_data);

  if (rc != SCSI_RC_GOOD) 
    return -1;

  scsi->total_block_count = ntohl(cap_data.total_block_count);
  scsi->block_size = ntohl(cap_data.block_size);

  return 0;
}

/*
 * Read a number of blocks from the drive
 */
static int scsi_read_write(int dir, int block_start, 
                           int block_count, char *data) {
  int transfer_len;
  int rc;

  /* Command to SCSI server on the USB mass storage device */
  struct command_descriptor_block10 cdb10 = {
    .op_code = (dir == SCSI_READ) ? CDB_READ10: CDB_WRITE10,
    .misc_CBD_and_service = 0,
    .logical_block_address = htonl(block_start),
    .misc_CBD = 0,
    .length = htons(block_count),  /* Unit of sectors */
    .control = 0
  };

  transfer_len = block_count * scsi->block_size;

  if (dir == SCSI_READ)
    rc = scsi->read(scsi->driver, sizeof(cdb10), (char *)&cdb10, 
                                  transfer_len, data);
  else
    rc = scsi->write(scsi->driver, sizeof(cdb10), (char *)&cdb10, 
                                   transfer_len, data);

  if (rc != SCSI_RC_GOOD)
    return -1;

  return 0;
}
/*
 * SCSI interface functions 
 */
int scsi_read(int block_start, int block_count, char *data) {
  int rc;

  spinlock_acquire(&scsi_dev_lock);

  if (_scsi_up() == SCSI_OFF) {
    spinlock_release(&scsi_dev_lock);
    return -1;
  }

  rc = scsi_read_write(SCSI_READ, block_start, block_count, data);
  spinlock_release(&scsi_dev_lock);

  return rc;
}

int scsi_write(int block_start, int block_count, char *data) {
  int rc;

  spinlock_acquire(&scsi_dev_lock);

  if (_scsi_up() == SCSI_OFF) {
    spinlock_release(&scsi_dev_lock);
    return -1;
  }

  rc = scsi_read_write(SCSI_WRITE, block_start, block_count, data);
  spinlock_release(&scsi_dev_lock);

  return rc;
}

