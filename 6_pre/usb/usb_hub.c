#include "usb.h"
#include "usb_hub.h"
#include "uhci.h"
#include "allocator.h"
#include "error.h"
#include "debug.h"
#include "list.h"
#include "../sleep.h"

DEBUG_NAME("USB HUB");

static struct list uhub_list_head;

static int usb_hub_init_port(struct usb_hub *, int port_nr);
static int usb_hub_destroy_port(struct usb_hub_port *port);
static void usb_hub_destroy_device(struct usb_hub_port *port);

/* 
 * USB HUB register functions
 *
 * Registered HUBs are scanned every 100ms for the port
 * status change
 */
struct usb_hub *usb_hub_init(struct usb_hub_reg *hub_reg) {
    struct usb_hub *uhub;
    int name_len;
    int i;

    DEBUG("Registering a usb hub: %s", hub_reg->name);
    DEBUG("           port count: %d", hub_reg->port_count);

    uhub = kzalloc(sizeof(struct usb_hub));
    if (uhub == NULL) {
        DEBUG("   ...failed");
        return NULL;
    }

    name_len = strlen(hub_reg->name) > 20 ?
        strlen(hub_reg->name) : 20;
    bcopy(hub_reg->name, uhub->name, name_len);

    uhub->hc = hub_reg->hc;
    uhub->hc_ops = hub_reg->hc_ops;
    uhub->hub = hub_reg->hub;
    uhub->hub_ops = hub_reg->hub_ops;

    /* Initialise a list of ports */
    LIST_INIT(&uhub->ports);
    for (i = 0; i < hub_reg->port_count; i++)
        usb_hub_init_port(uhub, i);

    LIST_LINK(&uhub_list_head, uhub);

    return uhub;
}

int usb_hub_destroy(struct usb_hub *uhub) {
    struct usb_hub_port *port, *port_h;

    LIST_UNLINK(uhub);

    LIST_FOR_EACH_SAFE(&uhub->ports, port, port_h) {
        DEBUG(" removing port %d", port->port_nr);
        LIST_UNLINK(port);
        usb_hub_destroy_port(port);
    }

    kfree(uhub);

    return 0;
}

/* 
 * USB HUB port interface, helper functionality
 */
static inline int usb_hub_port_disable(struct usb_hub_port *port) {
    struct usb_hub *uhub;
    uhub = port->hub;

    return uhub->hub_ops->port_disable(uhub, port->port_nr);
}

static inline int usb_hub_port_reset(struct usb_hub_port *port) {
    struct usb_hub *uhub;
    uhub = port->hub;

    return uhub->hub_ops->port_reset(uhub, port->port_nr);
}

static inline enum port_status_e 
usb_hub_port_status(struct usb_hub_port *port) {
    struct usb_hub *uhub;
    uhub = port->hub;

    return uhub->hub_ops->port_status(uhub, port->port_nr);
}

static inline enum usb_speed_class_e 
usb_hub_port_speed(struct usb_hub_port *port) {
    struct usb_hub *uhub;
    uhub = port->hub;

    return uhub->hub_ops->port_speed(uhub, port->port_nr);
}

/*
 * Allocates and disables a port on a given USB hub
 */
int usb_hub_init_port(struct usb_hub *uhub, int port_nr) {
    struct usb_hub_port *port;

    port = kzalloc(sizeof(struct usb_hub_port));

    if (port == NULL)
        return ERR_NO_MEM;

    /* Initialise port variables */
    port->status = USB_PORT_DISCONNECTED;
    port->hub = uhub;
    port->udev = NULL;
    port->power_consumption = 0;
    port->port_nr = port_nr;

    /* Disable USB port */
    usb_hub_port_disable(port);
    /* Link port on this USB hub */
    LIST_LINK(&uhub->ports, port);

    return 0;
}

int usb_hub_destroy_port(struct usb_hub_port *port) {
    /* If there is a device connected to this port, remove it */
    usb_hub_destroy_device(port);
    kfree(port);

    return 0;
}


/* 
 * USB hub functionality 
 */
static void usb_hub_initialise_device(struct usb_hub_port *port) {
#ifdef DEBUG_MODE
    const char speed[3][3]= {"LS", "FS", "HS"};
#endif
    struct usb_dev *udev;
    int address;
    int rc;
    int free = 0, alloc = 0;

    DEBUG("found a USB device");
    alloc_report_usage(&free, &alloc);
    DEBUG("Mem usage free %d\talloc %d", free, alloc);

    /* Perform standard reset routine before device initialisation */
    rc = usb_hub_port_reset(port);
    if (rc < 0) {
        DEBUG("reset failed");
        port->status = USB_PORT_DISABLED;
        usb_hub_port_disable(port);

        return;
    }

    /* Build new USB device */
    udev = kzalloc(sizeof(struct usb_dev));

    if (udev == NULL) {
        DEBUG("could not allocated memory for this device");
        port->status = USB_PORT_DISABLED;
        usb_hub_port_disable(port);

        return;
    }

    /* Populate udev structure */
    udev->port = port;
    udev->hc = port->hub->hc; /* USB device is reachable via
                               * the same host controller as this hub
                               */
    udev->hc_ops = port->hub->hc_ops;
    udev->hub = port->hub;


    /* Check USB device speed */
    udev->usb_sc = usb_hub_port_speed(port);
    address = (int)port->hub->hc_ops->get_next_addr(udev);
    DEBUG("port status %d device speed is %s address %d",
            (int)port->status, speed[(int)udev->usb_sc], address);

    /* Continue with USB configuration */
    rc = usb_configure_device(udev, address);

    if (rc < 0) {
        DEBUG("Failed to initialise USB device");
        usb_hub_port_reset(port);
        usb_configure_device(udev, address);
        usb_hub_port_disable(port);
        usb_hub_port_disable(port);
        kfree(udev);
        port->udev = NULL;
        port->status = USB_PORT_DISABLED;

        return;
    }

    port->status = usb_hub_port_status(port);
    port->udev = udev;

    DEBUG("USB device initialised successfully");
    DEBUG("  port %x, udev %x", port, port->udev);
}

static void usb_hub_destroy_device(struct usb_hub_port *port) {
    int free = 0, alloc = 0;
    if (port->udev == NULL) {
        DEBUG("Removing uninitialised device");
        return;
    }

    DEBUG("Removing device");
    usb_free_device(port->udev);
    kfree(port->udev);

    port->udev = NULL;

    /*
     * No need to update port status, since it is already
     * in the state USB_PORT_DISCONNECTED 
     */
    alloc_report_usage(&free, &alloc);
    DEBUG("Mem usage free %d\talloc %d", free, alloc);
}


/* 
 * Function scans all ports of all registered hubs
 * in the system.If a new device is found it is
 * initialised. 
 *
 * USB devices are initialised sequentially one by one,
 * thus only one device at the moment responses to 
 * the default address 0.
 */
void usb_hub_scan_ports() {
    struct usb_hub *uhub;
    struct usb_hub_port *port;
    enum port_status_e current_status;    /* Current port status */
    enum port_status_e previous_status;    /* Previous port status */

    /* Iterate over all registered hubs */
    LIST_FOR_EACH(&uhub_list_head, uhub) 
        /* Iterate over all ports of the hub */
        LIST_FOR_EACH(&uhub->ports, port) {
            /* If no change in the port status continue the loop */
            if (port->status == usb_hub_port_status(port))
                continue;

            previous_status = port->status;
            current_status = usb_hub_port_status(port);

            DEBUG("At hub %s port %d status change from %d to %d",
                    uhub->name, port->port_nr, previous_status, current_status);

            /* Before taking any action update the status */
            port->status = current_status; 

            /* Switch from the previous state to the next state */
            switch(previous_status) {
                case USB_PORT_DISCONNECTED:
                    /* Any change identifies a new device at the port */
                    usb_hub_initialise_device(port);
                    DEBUG(" ... done");
                    break;
                case USB_PORT_DISABLED:
                case USB_PORT_ENABLED:
                case USB_PORT_CONNECTED:
                case USB_PORT_SUSPENDED:
                    if (current_status == USB_PORT_DISCONNECTED) {
                        usb_hub_destroy_device(port);
                        break; 
                    }
            }
        }

}

/* 
 * USB device hub 
 *
 */

static int get_hub_descriptor(struct usb_hub_dev *uhd, 
        char *buff) {
    struct usb_generic_header ugh;
    int rc;

    struct usb_dev_setup_request get_description = {
        .bmRequestType = 0xA0,  /* GetHubDescriptor */
        .bRequest = GET_DESCRIPTOR,
        .wValue = (USB_HUB_DEV_DESCRIPTOR << 8),
        .wIndex = 0,
        .wLength = sizeof(struct usb_generic_header)
    };

    rc = usb_setup(uhd->udev, &get_description, USB_CREAD, 
            sizeof(struct usb_generic_header), (char *)&ugh);

    if (rc != 0)
        return ERR_XFER;

    get_description.wLength = ugh.bLength;

    rc = usb_setup(uhd->udev, &get_description, USB_CREAD,
            ugh.bLength, buff);

    if (rc != 0)
        return ERR_XFER;

    return ugh.bLength;
}

static int get_port_status(struct usb_hub_dev *uhd, int port_nr,
        uint16_t *port_status_change) {

    struct usb_dev_setup_request get_status = {
        .bmRequestType = 0xA3,  /* GetPortStatus */
        .bRequest = GET_STATUS,
        .wValue = 0,
        .wIndex = port_nr + 1,
        .wLength = 4
    };

    return usb_setup(uhd->udev, &get_status, USB_CREAD, 4, 
            (char *)port_status_change);
}

static int clear_port_feature(struct usb_hub_dev *uhd, int port_nr,
        uint16_t feature) {
    struct usb_dev_setup_request set_feature = {
        .bmRequestType = 0x23,  /* ClearPortStatus */
        .bRequest = CLEAR_FEATURE,
        .wValue = feature,
        .wIndex = port_nr + 1,
        .wLength = 0
    };

    return usb_setup(uhd->udev, &set_feature, USB_CWRITE, 0, NULL);
}

static int set_port_feature(struct usb_hub_dev *uhd, int port_nr,
        uint16_t feature) {
    struct usb_dev_setup_request set_feature = {
        .bmRequestType = 0x23,  /* SetPortStatus */
        .bRequest = SET_FEATURE,
        .wValue = feature,
        .wIndex = port_nr + 1,
        .wLength = 0
    };

    return usb_setup(uhd->udev, &set_feature, USB_CWRITE, 0, NULL);
}

/* Port status bits */
#define PORT_DEV_PRESENT  0x001
#define PORT_ENABLED      0x002
#define PORT_SUSPEND      0x004
#define PORT_POWER_STATUS 0x100
#define PORT_LOW_SPEED    0x200

/* Feature selectors */
#define PORT_ENABLE 1
#define PORT_RESET 4
#define PORT_POWER 8


static enum port_status_e 
uhd_port_status(struct usb_hub *uhub, int port_nr) {
    struct usb_hub_dev *uhd = (struct usb_hub_dev *)uhub->hub;
    uint16_t port_status_change[2] = {0,0};
    int rc;

    rc = get_port_status(uhd, port_nr, port_status_change);

    if (rc < 0)
        rc = USB_PORT_DISCONNECTED;
    else
        if ((port_status_change[0] & PORT_DEV_PRESENT) == 0)
            rc = USB_PORT_DISCONNECTED;
        else
            if ((port_status_change[0] & PORT_ENABLED) == 0)
                rc = USB_PORT_DISABLED;
            else
                if ((port_status_change[0] & PORT_SUSPEND) == 0)
                    rc = USB_PORT_ENABLED;
                else
                    rc = USB_PORT_SUSPENDED;

    DEBUG("Checking %s port %d status %x %x", uhub->name, port_nr,
            port_status_change[0], port_status_change[1]);

    return rc;
}

int uhd_port_disable(struct usb_hub *uhub, int port_nr) {
    struct usb_hub_dev *uhd = (struct usb_hub_dev *)uhub->hub;
    uint16_t port_status_change[2];

    clear_port_feature(uhd, port_nr, PORT_ENABLE);
    DEBUG("  Disable1: %x %x", port_status_change[0], port_status_change[1]);

    return 0;
}

int uhd_port_reset(struct usb_hub *uhub, int port_nr) {
    struct usb_hub_dev *uhd = (struct usb_hub_dev *)uhub->hub;

    set_port_feature(uhd, port_nr, PORT_RESET);

    return 0;
}

static enum usb_speed_class_e 
uhd_port_speed(struct usb_hub *uhub, int port_nr) {
    struct usb_hub_dev *uhd = (struct usb_hub_dev *)uhub->hub;
    uint16_t port_status_change[2];

    get_port_status(uhd, port_nr, port_status_change);

    if ((port_status_change[0] & PORT_LOW_SPEED) == 0)
        return USB_FS_DEV;
    else
        return USB_LS_DEV;
}

/* Hub operations */
struct usb_hub_ops uhd_hub_ops = {
    .port_speed = uhd_port_speed,
    .port_disable = uhd_port_disable,
    .port_reset = uhd_port_reset,
    .port_status = uhd_port_status
};

static int usb_hub_dev_init(struct usb_interface *uifc) {
    struct usb_hub_dev_descriptor *uhdd;
    struct usb_hub_dev *uhd;
    static char hub_name[] = "USB_DEV_HUB0";
    uint16_t port_status_change[2];
    char buff[512];
    int port_nr;
    int rc;

    DEBUG("Initialising USB hub device");

    uhd = kzalloc(sizeof(struct usb_hub_dev));

    if (uhd == NULL)
        return ERR_NO_MEM;

    uhd->udev = uifc->udev;
    uhd->uifc = uifc;
    uifc->driver_data = (void *)uhd;

    /* Get hub descriptor */
    rc = get_hub_descriptor(uhd, buff);

    if (rc < 0) 
        goto uhd_init_fail;

    uhdd = (struct usb_hub_dev_descriptor *)buff;

    /* Hub description */
    struct usb_hub_reg hub = {
        .name = hub_name,             /* Hub name */
        .hc = (void *)uhd->udev->hc,  /* Host controller used to reach this hub */
        .hc_ops = uhd->udev->hc_ops,  /* Host controller options */
        .hub = (void *)uhd,           /* State of this hub */
        .hub_ops = &uhd_hub_ops,      /* Options of this hub */
        .port_count = uhdd->bNbrPorts /* Number of ports on this hub */
    };

    /* Register this hub on the hub list */
    uhd->uhub = usb_hub_init(&hub);

    if (uhd->uhub == NULL)
        goto uhd_init_fail;

    /* Update name for the next hub */
    hub_name[11]++;

    /* 
     * If hub ports support power switch off 
     * then we must enable power on the ports 
     * to detect connected devices 
     */
    for (port_nr = 0; port_nr < uhdd->bNbrPorts; port_nr++) {
        /* Get port status */
        rc = get_port_status(uhd, port_nr, port_status_change);
        if ((port_status_change[0] & PORT_POWER_STATUS) == 0) {
            DEBUG("Powering port %d", port_nr);
            set_port_feature(uhd, port_nr, PORT_POWER);
        }
    }

    return 0;

uhd_init_fail:
    kfree(uhd);
    return ERR_NO_DRIVER;
}

static void usb_hub_dev_free(struct usb_interface *uifc) {
    struct usb_hub_dev *uhd;

    uhd = (struct usb_hub_dev *)uifc->driver_data;

    /* Destroy this hub */
    usb_hub_destroy(uhd->uhub);

    /* Deallocated hub resources */
    kfree(uhd);
}


static struct usb_dev_driver usb_hub_dev_driver = {
    .class_code = 0x09,
    .init = usb_hub_dev_init,
    .free = usb_hub_dev_free
};

int usb_hub_static_init() {
    LIST_INIT(&uhub_list_head);

    usb_dev_driver_register(&usb_hub_dev_driver);

    return 0;
}
