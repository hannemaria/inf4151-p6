#ifndef EHCI_PCI_H
#define EHCI_PCI_H

void ehci_pci_dev_driver_register();

struct ep_usbbase{
    const uint32_t res1:1;
    const uint32_t type:2;
    const uint32_t res2:5;
    uint32_t bAddr:24;
}__attribute__((packed));

struct ep_portwakereg{
    uint32_t reg:16;
}__attribute__((packed));

struct ep_usblegsup{
    const uint32_t cap_ID:8;
    const uint32_t next_eecp:8;
    uint32_t hc_bios_own:1;
    const uint32_t res:7;
    uint32_t hc_os_own_sem:1;
    const uint32_t res2:7;
}__attribute__((packed));

#define EHCI_USBLEGSUP_HCOSOWN_MASK (1 << 24)
#define EHCI_USBLEGSUP_HCOSOWN_SEM(reg) \
    (reg & (1 << 24)) >> 24
#define EHCI_USBLEGSUP_HCBIOWN_MASK (1 << 16)
#define EHCI_USBLEGSUP_HCBIOWN_SEM(reg) \
    (reg & (1 << 16)) >> 16
#define EHCI_USBLEGSUP_NEXTEXT_MASK (0xff << 8)
#define EHCI_USBLEGSUP_NEXTEXT_PTR(reg) \
    (reg & (0xff << 8)) >> 8
#define EHCI_USBLEGSUP_CAPABID_MAST (0xff)
#define EHCI_USBLEGSUP_CAPABID(reg) \
    (reg & 0xff)

#define EHCI_PCIREG_BASE 0x10
#define EHCI_PCIPORTWAKECAP 0x62
#define EHCI_MAX_REG_LEN 152

/* 
 * Host controler capability registers (HCCREG) 
 * are located in address memory starting 
 * from address pointed to by EHCI_PCIREG_BASE
 */
#define EHCI_HCCREG_CAPLENGTH   0x00  /* 8 bits reg */
#define EHCI_HCCREG_HCIVERSION  0x02
#define EHCI_HCCREG_HCSPARAMS   0x04
#define EHCI_HCCREG_HCCPARAMS   0x08  /* 32 bits reg */
#define EHCI_HCCREG_HCSP_PROUTE 0x0c /* 64 bit reg */

/*
 * Host controler operational registers (OPREG)
 * are located in address memory starting from 
 * address pointed to by 
 *  EHCI_PCIREG_BASE + EHCI_HCCREG_CAPLENGTH
 */
#define EHCI_OPREG_USBCMD     0x00  /* 32 bits reg */
#define EHCI_OPREG_USBSTS       0x04
#define EHCI_OPREG_USBINTR      0x08
#define EHCI_OPREG_FRINDEX      0x0c
#define EHCI_OPREG_CTRLDSSEG    0x10
#define EHCI_OPREG_PERLISTBASE  0x14
#define EHCI_OPREG_ASYNCLISTADR 0x18
#define EHCI_OPREG_CONFIGFLAG   0x40
#define EHCI_OPREG_PORTSC       0x44

#endif
