#ifndef USB_HUB_H
#define USB_HUB_H

#include "usb.h"
#include "list.h"

enum port_status_e {
  USB_PORT_DISCONNECTED = 0,
  USB_PORT_CONNECTED,
  USB_PORT_DISABLED,
  USB_PORT_ENABLED,
  USB_PORT_SUSPENDED
};

struct usb_hub_port {
  struct list list;
  enum port_status_e status;      /* Port status */
  struct usb_dev *udev;           /* Device attached to this port */
  struct usb_hub *hub; 	          /* USB hub this port is connected to */
  int power_consumption;          /* Power consumption of this port */
  int port_nr;                    /* Port number at the hub */
};

struct usb_hub {
  struct list list;     /* List handle for linking USB hubs */
  struct list ports;    /* List head for linking USB hub ports */

  /* Host controller associated with this hub */
  char name[20];
  void *hc;                       /* host controller state */
  struct usb_hc_ops *hc_ops;      /* host controller operations */

  /* This hub data and operations */
  void *hub;
  struct usb_hub_ops *hub_ops;

//  struct usb_hub *upstream_hub; 
//  struct list downstram_hub;

//  int power_provided;
};

/* This is a form used to register a hub */
struct usb_hub_reg {
  char *name;
  /* Host controller section */
  void *hc;
  struct usb_hc_ops *hc_ops;
  /* Hub section */
  void *hub;
  struct usb_hub_ops *hub_ops;
  int port_count;
};

/* Functions to export */
int usb_hub_static_init();

struct usb_hub *usb_hub_init(struct usb_hub_reg *);
int usb_hub_destroy(struct usb_hub *);
void usb_hub_scan_ports();

/* USB hub operations */
struct usb_hub_ops {
  enum usb_speed_class_e (*port_speed)(struct usb_hub *, int port);
  enum port_status_e     (*port_status)(struct usb_hub *, int port);
  int                    (*port_reset)(struct usb_hub *, int port);
  int                    (*port_disable)(struct usb_hub *, int port);
};

/* USB hub device */
struct usb_hub_dev {
  struct usb_dev *udev;
  struct usb_interface *uifc;

  struct usb_hub *uhub;
  int port_count;
};

/* USB hub device standard descriptor */
struct usb_hub_dev_descriptor {
  uint8_t bDescLength;
  uint8_t bDescriptorType;
#define USB_HUB_DEV_DESCRIPTOR 0x29
  uint8_t bNbrPorts;
  uint16_t wHubCharacteristics;
  uint8_t bPwrOn2PwrGood;
  uint8_t bHubContrCurrent;
  uint8_t DeviceRemovable[0];
// right after DeviceRemoveable  PortPwrCtrlMask
};

#endif
