
#include "../memory.h"
#include "../thread.h"
#include "allocator.h"
#include "error.h"
#include "asyncq.h"
#include "pci.h"
#include "ehci.h"
#include "ehci_pci.h"
#include "usb_hub.h"
#include "debug.h"

DEBUG_NAME("EHCI");

static void printQtd(volatile struct aqtd *td, int is_64_bit){
  DEBUG("    Printing QTD %x", td);
  DEBUG("    nextQtd");
  DEBUG("     T: %d",         td->nextQtd.T);
  DEBUG("     ptr: %x",       td->nextQtd.nextQtdPtr);
  DEBUG("    nextAltQtd");
  DEBUG("     T: %d",         td->nextAltQtd.T);
  DEBUG("     ptr: %x",       td->nextAltQtd.nextQtdPtr);
  DEBUG("    status bits");
  DEBUG("     Active: %s",                td->status & EHCI_STATUS_ACTIVE ? "Yes" : "No");
  DEBUG("     Halted:: %s",               td->status & EHCI_STATUS_HALTED ? "Yes" : "No");
  DEBUG("     Data Buffer Error: %s",     td->status & EHCI_STATUS_DATA_BUFFER ? "Yes" : "No");
  DEBUG("     Babble: %s",                td->status & EHCI_STATUS_BABBLE ? "Yes" : "No");
  DEBUG("     Transaction Error: %s",     td->status & EHCI_STATUS_XACTERR ? "Yes" : "No");
  DEBUG("     Missed microframe: %s",     td->status & EHCI_STATUS_MISS_UF ? "Yes" : "No");
  DEBUG("     Split trstate: %s",         td->status & EHCI_STATUS_SPLIT ? "Yes" : "No");
  DEBUG("     Ping state: %s",            td->status & EHCI_STATUS_PING ? "Yes" : "No");
  DEBUG("    PID: %x",        td->pidCode);
  DEBUG("    Cerr: %d",       td->cerr);
  DEBUG("    C_page: %d",     td->c_page);
  DEBUG("    ioc: %d",        td->ioc);
  DEBUG("    Tot. bytes to transfer: %d", td->totTransLen);
  DEBUG("    current offset: %x",         td->bufferPtrOff.curOffset);
  DEBUG("    Buffer pointers");
  DEBUG("    %x",             td->bufferPtrOff.bufPtrPage);
  /*  
  DEBUG("    %x",             td->bufferPtr[0].bufPtrPage);
  DEBUG("    %x",             td->bufferPtr[1].bufPtrPage);
  DEBUG("    %x",             td->bufferPtr[2].bufPtrPage);
  DEBUG("    %x",             td->bufferPtr[3].bufPtrPage);
  */

  if(is_64_bit){

    DEBUG("    Extended Buffer pointers");
    DEBUG("    %x",             td->extBufPtr[0].upperAddr);
    DEBUG("    %x",             td->extBufPtr[1].upperAddr);
    DEBUG("    %x",             td->extBufPtr[2].upperAddr);
    DEBUG("    %x",             td->extBufPtr[3].upperAddr);
    DEBUG("    %x",             td->extBufPtr[4].upperAddr);
  }

}

/*
 * Frees the memory occupied from a linked list of queue
 * transmission descriptors. 
 *
 * - eh:        pointer to ehci main data structure
 * - aqtd:      pointer to very first queue transmission
 *              descriptor of queue head
 * - aqHead:    queue head to free
 *
 * Only considers 'next qtd pointer' (i.e., NOT 'alternative
 * qtd pointer').
 *
 * Software must ensure host controller cache is coherent
 * before attempting to free this memory (see doorbell
 * feature).
 */
static void dallocQHead(volatile struct ehci *eh, volatile struct aqtd *aqtd, volatile struct aqhead *aqHead){

  volatile struct aqtd *tempAqTdPtr;
  int i, j = 0;

  /*
   * Handshake with EHCI controller. We do this to verify
   * that the HC doesn't keep any cached state (e.g.,
   * a pointer) about the queue head to be removed. 
   *
   * Doorbell mechanism works as follows:
   * 1) Set command bit interrupt on async advance
   * 2) When cached state is cleared, HC will set
   *    Interrupt on Async Advance bit in status register.
   *    It will generate an interrupt IF Interrupt on
   *    Async Advance bit is set to '1'. But we don't use
   *    interrupts, we just busy wait on IAA bit in status
   *    register.
   * 3) Clear Interrupt on Async Advance bit in status
   *    register. 
   */

  eh->opReg->command.intAsDB      =       1;
  while(!eh->opReg->usbStat.intAsAdv);

  eh->opReg->usbStat.intAsAdv     =       0;

  /*
   * We're free to do whatever we want with unlinked queue
   * heads, so let's free them and associated queue
   * transmission descriptors.
   */

  do{

    tempAqTdPtr     =   (struct aqtd*)((uint32_t) aqtd->nextQtd.nextQtdPtr << 5);
    i               =   (int) aqtd->nextQtd.T;

    kfree((struct aqtd*) aqtd);

    aqtd            =   tempAqTdPtr;
    j++;
  }while(i == 0);


  kfree((struct aqhead*) aqHead);

}

/*
 * A very simple implementation of asynchronous transfers
 * using the EHCI controller. 
 * -> It only supports USB 2.0 reads/writes.
 * -> Only one transmission is executed at a time. 
 *
 * When the EHCI async queue is not being read or written to
 * by (implicit use) of this function, there is always
 * a single element in the EHCI async queue. This element
 * is inactive, has its H-bit set, and only serves to keep
 * the async traversal active. 
 *
 * When this function is called (usually as part of
 * ehci_read/write/setup), the driver simply claims a queue
 * lock to prevent race conditions in the form of concurrent
 * operations on the queue. The lock is held until the
 * function completes its work, with or without errors.
 *
 * In order to perform the requested operation, a single
 * queue head is allocated and initialized. Subsequently,
 * transmission descriptors are created to perform the full
 * transfer of 'size' bytes (each descriptor can transfer
 * five pages, or around 20 kB).
 *
 * The queue head w./trans. descr. is inserted into the
 * (empty) queue. Then, the function simply performs buzy 
 * waiting to determine when the function completes. 
 *
 * When the function is complete, the transmission complete
 * status is checked. Queue head and trans. descr. are
 * freed, and appropriate error value is passed back to
 * caller. Finally, lock is released. 
 */
static int ehci_read_write(struct usb_pipe *pipe, int pid, int size, char *data) {

  volatile struct ehci *eh = pipe->udev->hc;
  volatile struct aqhead *aqHeadPtr;
  volatile struct aqtd    *aqTdPtr, 
           *firstAqTdPtr, 
           *tempAqTdPtr;
  int i, j,
      toTransfer,
      transferred;
  unsigned int pidCode;

  i = 0;
  toTransfer = 0;
  transferred = size;

  lock_acquire((lock_t*)&eh->asq_lock);

  /* Must be aligned to 32 bit */
  aqHeadPtr           =   kzalloc_align(sizeof (struct aqhead), 0x20);

  if(aqHeadPtr == NULL){
    DEBUG("    Out of memory");
    lock_release((lock_t*)&eh->asq_lock);
    return ERR_NO_MEM;
  }

  aqHeadPtr->T            =   0;                          // N/A
  aqHeadPtr->llTyp        =   ASYNCQ_LLTYP_QHEAD;
  aqHeadPtr->hPointer     =   (uint32_t) eh->asyncHead >> 5;
  aqHeadPtr->RL           =   15;                          // No NAK retries
  aqHeadPtr->C            =   0;                          // Set to 0 for high-speed device
  aqHeadPtr->maxPcktLen   =   pipe->max_packet_size;      // Is this correct?
  aqHeadPtr->H            =   1;
  aqHeadPtr->dtc          =   1;                          // DT source
  aqHeadPtr->eps          =   0x2;                        // End point speed= full speed
  aqHeadPtr->endPt        =   pipe->ep_address;
  aqHeadPtr->I            =   0;                          // N/A
  aqHeadPtr->devAddr      =   pipe->udev->address;
  aqHeadPtr->mult         =   0x3;                        // Three transactions per microframe
  aqHeadPtr->portNum      =   0;                          // N/A
  aqHeadPtr->hubAddr      =   0;                          // N/A
  aqHeadPtr->Cmask        =   0;                          // N/A
  aqHeadPtr->Smask        =   0;                          // Set to 0 for async queue

  /*
   * Done setting up queue head. 
   * Setup transfer descriptor(s).
   */


  switch(pid){

    case EHCI_PID_OUT:
      pidCode        =   0;
      break;

    case EHCI_PID_IN:
      pidCode        =   1;
      break;

    case EHCI_PID_SETUP:
      pidCode        =   2;
      break;

    default:
      DEBUG("    Unknown PID code");

      kfree((struct aqhead*) aqHeadPtr);
      lock_release((lock_t*)&eh->asq_lock);

      return -1;
  }

  /*
   * For very first use of queue head, it is sufficient to
   * zero out the data structure, then set pointers to
   * valid QTDs.
   *
   * (EHCI specification p.80)
   *
   * 'firstAqTdPtr' represents the very first queue
   * transmission descriptor. If more are needed (i.e., total
   * size is higher than 20kB), additional transmission
   * descriptors will be allocated.
   */
  firstAqTdPtr = kzalloc_align(sizeof(struct aqtd), 0x20);
  aqHeadPtr->qtd.nextAltQtd.T         =   1;
  aqHeadPtr->qtd.nextQtd.T            =   0;
  aqHeadPtr->qtd.nextQtd.nextQtdPtr   =   (uint32_t) firstAqTdPtr >> 5;

  /*
   * This do-while loop creates as many transmission
   * descriptors as are necessary to transfer 'size'
   * bytes and links them together.
   */
  aqTdPtr                         =   firstAqTdPtr;
  j = 0;
  do{
    /*
     * toTransfer marks the number of bytes to transfer
     * with THIS queue transmission descriptor.
     */
    toTransfer = ((int) (size / EHCI_QTD_MAXTRANS_SIZE) > 0 ? 
        EHCI_QTD_MAXTRANS_SIZE : size % EHCI_QTD_MAXTRANS_SIZE);

    size -= toTransfer;

    aqTdPtr->nextAltQtd.T   =   1;
    aqTdPtr->dt             =   pipe->toggle_bit;
    pipe->toggle_bit        ^=  1;
    aqTdPtr->totTransLen    =   toTransfer;
    aqTdPtr->ioc            =   0;
    aqTdPtr->c_page         =   0;
    aqTdPtr->cerr           =   0x3;
    aqTdPtr->pidCode        =   pidCode;

    /*
     * What should we set PING flag to?
     */
    if(pid == EHCI_PID_OUT){

      aqTdPtr->status         =   
        QTD_STATUS_ACTIVE | 
        QTD_STATUS_PINGST;


    }else{
      aqTdPtr->status         =   
        QTD_STATUS_ACTIVE;
    }

    /*
     * Write page pointer array.
     */
    aqTdPtr->bufferPtrOff.bufPtrPage    =   (uint32_t) data >> 12;
    aqTdPtr->bufferPtrOff.curOffset     =   (uint32_t) data % PAGE_SIZE;

    /* 
     * Page align data pointer.
     */
    data += PAGE_SIZE;
    data = (char*) ((uint32_t) data & ~0xfff);

    if(toTransfer != 0)
      toTransfer -= PAGE_SIZE - (uint32_t) data % PAGE_SIZE;

    for(i = 0; i < 4; i++){
      if(toTransfer <= 0)
        break;

      aqTdPtr->bufferPtr[i].bufPtrPage    
        =   (uint32_t) data >> 12;    // Data must be page aligned here
      aqTdPtr->bufferPtr[i].res
        =   0;                  // Reserved bits should be set to 0
      data        +=  PAGE_SIZE;
      toTransfer  -=  PAGE_SIZE;
    }


    //            printQtd(aqTdPtr, eh->capReg->capabiPar.addCap64);

    /*
     * Allocate a new transmission descriptor, if
     * necessary.
     */
    if(size > 0){

      tempAqTdPtr                 =   kzalloc_align(sizeof(struct aqtd), 0x20);
      aqTdPtr->nextQtd.nextQtdPtr =   (uint32_t) tempAqTdPtr & ~0x1f;
      aqTdPtr->nextQtd.T          =   0;
      aqTdPtr                     =   tempAqTdPtr;
    }
    else{

      aqTdPtr->nextQtd.T          =   1;
    }

  }while(size > 0);

  /*
   * Queue head and associated transmission descriptors
   * should be setup. Insert into queue...
   */

  aqHeadPtr->hPointer     =   eh->asyncHead->hPointer;
  eh->asyncHead->hPointer =   (uint32_t) aqHeadPtr >> 5;
  eh->asyncHead->H        =   0;

  /*
   * ...and busy wait until HC is done
   */

  aqTdPtr     =   firstAqTdPtr;
  i           =   0;

  do{
    /*
     * It has been observed that for some USB drives, the
     * driver hangs here
     */
    uint64_t start, end;

    start = get_timer();
    end = start + ms_to_cycles(100);
    while(aqTdPtr->status & QTD_STATUS_ACTIVE){

      /*
       * Check if a timeout occurred
       * */
      if(get_timer() > end){
        // Timeout
        DEBUG("Timeout");
        DEBUG("ASYNC LIST ADDRESS %x", *((uint32_t*) &eh->opReg->asyncListBase));
        DEBUG("Asynq head %x, next %x", eh->asyncHead, eh->asyncHead->hPointer << 5);
        DEBUG("This queue head %x, next %x", aqHeadPtr, aqHeadPtr->hPointer << 5);
        while(1);
      }
    }

    /*
     * Error check 
     */
    if(aqTdPtr->status & QTD_STATUS_HALTED){

      DEBUG("    Queue transmission error. (%d)", eh->capReg->capabiPar.addCap64);
      printQtd(aqTdPtr, eh->capReg->capabiPar.addCap64);

      /*
       * Clean up
       */
      dallocQHead(eh, firstAqTdPtr, aqHeadPtr);

      lock_release((lock_t*)&eh->asq_lock);
      return -1;
    }

    /*
     * No error, proceed to next QTD (if any)
     *
     * Never use aqTdPtr after this point. It will be
     * invalid for the last descriptor.
     */

    pipe->toggle_bit    =   aqTdPtr->dt;

    j           =   aqTdPtr->nextQtd.T;
    aqTdPtr     =   (volatile struct aqtd*)
      ((uint32_t) aqTdPtr->nextQtd.nextQtdPtr << 5);
    i++;

  }while(j == 0);


  /*
   * Transfer is complete and successful, remove from
   * queue.
   *
   * There is no need to set "this" (queue head to be
   * removed) horizontal pointer as long as we only remove
   * one head.
   */

  eh->asyncHead->H         =   1;
  eh->asyncHead->hPointer  =   (uint32_t) eh->asyncHead >> 5;

  dallocQHead(eh, firstAqTdPtr, aqHeadPtr);

  /*
   * Finally, release lock and return.
   */
  lock_release((lock_t*)&eh->asq_lock);
  return transferred;
}

/* Standard read, write and setup EHCI functions */
static int ehci_read(struct usb_pipe *pipe, int size, char *data) {

  DEBUG("    Entered ehci_read()");
  return ehci_read_write(pipe, EHCI_PID_IN, size, data);
}

static int ehci_write(struct usb_pipe *pipe, int size, char *data) {

  DEBUG("    Entered ehci_write()");
  return ehci_read_write(pipe, EHCI_PID_OUT, size, data);
}

static int ehci_setup(struct usb_pipe *pipe, struct usb_dev_setup_request *data) {

  /* 
   * Setup packet size is always 8B 
   *
   * True for USB 2.0?
   * */
  DEBUG("    Entered ehci_setup()");
  return ehci_read_write(pipe, EHCI_PID_SETUP, 8, (char *)data);  
}

/* This function just checks the connected bit of the
 * associated portsc register. The port connection change
 * status bit is left unchecked. 
 *
 * If 
 *  -> the port number doesn't exist
 *  -> the EHCI controller does not own the port
 *  this function will
 * return "status disconnected". 
 *
 * Might have to implement some nicer return codes. */
enum port_status_e ehci_port_status(struct usb_hub *uhub, int port){

  //    uint32_t status;
  volatile struct hcPortsc *hcport;
  volatile struct ehci *eh = (struct ehci *)uhub->hc;

  if(port < 0 || port >= eh->capReg->structPar.nPorts){

    DEBUG("    Attempted to read a nonexistent port.");
    return USB_PORT_DISCONNECTED;
  }

  /* Read the PORTSC register */
  //    status = *((uint32_t *)(&eh->opReg->portsc[port]));

  hcport = &eh->opReg->portsc[port];

  /* Check if the EHCI controller owns this port */
  //    if(status & EHCI_PTC_POWNER){
  if(hcport->portOwn){

    return USB_PORT_DISCONNECTED;
  }

  /* If connection state has changed and device is connected */
  //    if(status & EHCI_PTC_CURCONSTAT){
  if(hcport->curConSt){

    return USB_PORT_CONNECTED;

  }
  //    else if(!(status & EHCI_PTC_CURCONSTAT)){
  else if(!hcport->curConSt){

    return USB_PORT_DISCONNECTED;
  }
  else{

    DEBUG("    Ask OS admin for help (unknown connection state detected).");
    return USB_PORT_DISCONNECTED;
  }

}

/*
 * ToDo:
 * Implement bitmap allocator
 *
 * Piotr says:
 * then we also need deallocator
 */
static uint8_t ehci_get_address(struct usb_dev *udev) {
  struct ehci *eh = (struct ehci *)udev->hc;
  uint8_t address;

  address = eh->next_address;
  eh->next_address++;

  return address;
}

/*
 * Disables a host controller port. 
 */
int ehci_port_disable(struct usb_hub *uhub, int port) {

  volatile struct ehci *eh = (volatile struct ehci *)uhub->hc;

  if(port < 0 || port >= eh->capReg->structPar.nPorts){

    return USB_PORT_DISCONNECTED;
  }

  /*
   * Simply clear status bit, and wait for it to clear.
   */

  if(!eh->opReg->portsc[port].portEn){
    return 0;
  }

  eh->opReg->portsc[port].portEn = 1;
  while(eh->opReg->portsc[port].portEn);

  return 0;
}

/*
 * This function resets the requested port of the EHCI
 * controller as part of device initialization or recovery
 * operations.
 *
 * This function should be called by USB driver when it 
 * detects a port status change (PORT_STATUS_CONNECTED). USB 
 * driver can detect this using the ehci_port_status(..) call. 
 *
 * EHCI driver needs to determine the port speed of the
 * connected device (USB 1.x or 2.0). If a USB 1.x device is
 * detected, EHCI driver will perform port handover to
 * companion (UHCI) controller. If a 2.0 device is detected,
 * EHCI controller will complete device reset by claiming
 * ownership of device and returning a success value to USB
 * driver.
 *
 * For full (USB 1.x) or high speed (2.0) devices, EHCI
 * driver will perform unconditional device reset in order
 * to complete device speed detection. 
 */
int ehci_port_reset(struct usb_hub *uhub, int port) {

  volatile struct ehci *eh = (volatile struct ehci *)uhub->hc;

  int i;

  if(port < 0 || port >= eh->capReg->structPar.nPorts){

    DEBUG("    Attempted to read a nonexistent port.");
    return -1;
  }

  /* Need to determine the speed of the device:
   *
   *  - low speed  (USB 1.0)
   *  - full speed (USB 1.0)
   *  - high speed (USB 2.0)
   *  
   *  else the port is released to CC. 
   */
  if(eh->opReg->portsc[port].lineStat & 0x01){       // Was low speed

    DEBUG("    Low speed device. Releasing ownership..");
    eh->opReg->portsc[port].portOwn = 1;

    return -1;

  }else{

    DEBUG("    Found high/full speed device. Resetting..");

    enter_critical();

    /* full / high speed device - perform reset */
    eh->opReg->portsc[port].portRes = 1;
    eh->opReg->portsc[port].portEn = 0;

    ms_delay(100);

    /* Write '0' to reset, wait for value to be written */
    eh->opReg->portsc[port].portRes = 0;

    i=0;
    DEBUG("    Waiting for bit clear..");
    while(eh->opReg->portsc[port].portRes != 0) i++;
    DEBUG("    Cleared (in %d iCycles).", i);

    leave_critical();

    /* Device is now reset, check if high-speed */
    if(eh->opReg->portsc[port].portEn == 1){

      DEBUG("    High speed device detected.");
      return 0;

    }else{

      DEBUG("    Full speed device detected. Releasing ownership..");

      eh->opReg->portsc[port].portOwn = 1;
      return -1;
    }


  }



  return 0;
}

/* 
 * Caller should check for negative return value 
 * */
enum usb_speed_class_e ehci_port_speed(struct usb_hub *uhub, int port) {

  volatile struct ehci *eh = uhub->hc;

  if(port < 0 || port >= eh->capReg->structPar.nPorts){

    DEBUG("    Attempted to read a nonexistent port.");
    return -1;
  }

  // Just check if the port is enabled in PORTSC
  if(eh->opReg->portsc[port].portEn == 1)
    return USB_HS_DEV;

  return -1;
}

/*
 * Unused.
 */
static int ehci_register_interrupt_h(struct usb_interrupt *ui) {

  return 0;
}


/* 
 * Hub operations 
 * */
struct usb_hub_ops ehci_hub_ops = {
  .port_speed = ehci_port_speed,
  .port_reset = ehci_port_reset,
  .port_status = ehci_port_status,
  .port_disable = ehci_port_disable
};

/* 
 * Host controller operations 
 * */
struct usb_hc_ops ehci_hc_ops = {
  .read = ehci_read,
  .write = ehci_write,
  .setup = ehci_setup,
  .get_next_addr = ehci_get_address,
  .register_interrupt_h = ehci_register_interrupt_h
};

/*
 * Debug function to read EHCI controller capabilities.
 */
#if 0
static void printCapRegs(volatile struct ehci *eh){

  DEBUG("EHCI capability registers");
  DEBUG("    Capability length %d", eh->capReg->capLen);
  DEBUG("    Version %d:%d", eh->capReg->version.major, eh->capReg->version.minor);
  DEBUG("    STRUCTURAL");
  DEBUG("    numPorts %d", eh->capReg->structPar.nPorts);
  DEBUG("    PPC: %d", eh->capReg->structPar.PPC);
  DEBUG("    PRR: %d", eh->capReg->structPar.PRR);
  DEBUG("    NPCC: %d", eh->capReg->structPar.NPCC);
  DEBUG("    NCC: %d", eh->capReg->structPar.NCC);
  DEBUG("    PIND: %d", eh->capReg->structPar.PIND);
  DEBUG("    Debug port number: %d", eh->capReg->structPar.dbgPrtN);
  DEBUG("    CAPABILITY");
  DEBUG("    64 bit enabled: %s", eh->capReg->capabiPar.addCap64 ? "Yes" : "No");
  DEBUG("    Programmable frame list: %s", eh->capReg->capabiPar.prgFrmLstFlg ? "Yes" : "No");
  DEBUG("    Asynch. sched park capable: %s", eh->capReg->capabiPar.asSchPC ? "Yes" : "No");
  DEBUG("    Isochronous sched. threshold: %x", eh->capReg->capabiPar.isoSchThr);
  DEBUG("    EHCI Extended Capability: %x", eh->capReg->capabiPar.EHCIExtCap);
}
#endif

/*
 * EHCI controller device initialization routine.
 *
 * This function is called once for all EHCI controllers
 * detected during PCI device enumeration at OS boot. 
 *
 *
 */
int ehci_init(volatile struct ehci *eh) {

  static char hub_name[] = "EHCI_HUB0";
  //    uint16_t portWakeCap;
  uint32_t usblegsup_offset, usblegsup;
  int i;
  struct usb_hub *uhub;
  volatile struct aqhead *aqHeadPtr;

  void *regBase;


  eh->next_address = 1;       /* BAD! */

  /* EHCI registers are located in memory address space */
  regBase         =   (uint8_t *) (pci_read_dev_reg32(eh->pci, EHCI_PCIREG_BASE) & 0xffffff00);
  //    portWakeCap     =   (uint16_t)   pci_read_dev_reg16(eh->pci, EHCI_PCIPORTWAKECAP);

  eh->capReg      =   (volatile struct hcCapReg*) regBase;
  eh->opReg       =   (volatile struct  hcOpReg*) (regBase + eh->capReg->capLen);

  /*
   * Perform host controller reset
   */
  eh->opReg->command.reset = 1;
  while (eh->opReg->command.reset == 1);

  /* 
   * Need to do bios handover to claim ownership over host
   * controller.
   */
  usblegsup_offset    =   eh->capReg->capabiPar.EHCIExtCap;
  while(usblegsup_offset != 0){

    usblegsup = pci_read_dev_reg32(eh->pci, usblegsup_offset);

    DEBUG("usblegsupoffset %d", usblegsup_offset);
    DEBUG("usblegsup %x", usblegsup);

    /*
     * If BIOS has "claimed" host controller, need to
     * reclaim.
     */
    if(EHCI_USBLEGSUP_HCBIOWN_SEM(usblegsup) && (EHCI_USBLEGSUP_CAPABID(usblegsup) == 0x1)){

      DEBUG("Reclaiming EHCI controller from BIOS...");

      pci_write_dev_reg32(
          eh->pci, 
          usblegsup_offset, 
          usblegsup | EHCI_USBLEGSUP_HCOSOWN_MASK
          );

      DEBUG("Waiting for EHCI controller ownership..");
      while(!(pci_read_dev_reg32(eh->pci, usblegsup_offset) & EHCI_USBLEGSUP_HCOSOWN_MASK));
      DEBUG("Waiting for BIOS to release EHCI ownership..");
      DEBUG("(This process takes some time in rare cases..)");
      DEBUG("(Please contact TA or OS administrator if the driver hangs here indefinitely.)");
      while(pci_read_dev_reg32(eh->pci, usblegsup_offset) & EHCI_USBLEGSUP_HCBIOWN_MASK);
      DEBUG("Host controller BIOS owned was cleared, continuing.");
    }

    /*
     * Reclaimed host controller, get next USBLEGSUP
     * register offset. The while loop will stop
     * traversing these registers when the offset is
     * zero.
     */
    usblegsup_offset =
      EHCI_USBLEGSUP_NEXTEXT_PTR(pci_read_dev_reg32(eh->pci, usblegsup_offset));

  }

  /* We always work in first 4 GB of memory, regardless of 64 bit register
   * setting (so no need to set CTRLDSSEGMENT) */

  /* All interrupts disabled by default */

  aqHeadPtr           =   kzalloc_align(sizeof (struct aqhead), 0x20);
  aqHeadPtr->qtd.status   |=   QTD_STATUS_HALTED;
  aqHeadPtr->T        =   0;                          // N/A
  aqHeadPtr->llTyp    =   ASYNCQ_LLTYP_QHEAD;
  aqHeadPtr->hPointer =   (uint32_t) aqHeadPtr >> 5;
  aqHeadPtr->RL       =   0;                          // No NAK retries
  aqHeadPtr->C        =   0;                          // Set to 0 for high-speed device
  aqHeadPtr->H        =   1;
  aqHeadPtr->dtc      =   0;                          // ??
  aqHeadPtr->eps      =   0x2;                        // End point speed= full speed
  aqHeadPtr->endPt    =   0;                          // N/A
  aqHeadPtr->I        =   0;                          // N/A
  aqHeadPtr->devAddr  =   0;                          // N/A
  aqHeadPtr->mult     =   0x3;                        // Three transactions per microframe
  aqHeadPtr->portNum  =   0;                          // N/A
  aqHeadPtr->hubAddr  =   0;                          // N/A
  aqHeadPtr->Cmask    =   0;                          // N/A
  aqHeadPtr->Smask    =   0;                          // Set to 0 for async queue

  /*
   * Just set T bits of transmission descriptors to 1.
   * This should invalidate the pointers, and make EHCI
   * controller skip to next queue head.
   */
  aqHeadPtr->qtd.nextQtd.T            =   1;
  aqHeadPtr->qtd.nextAltQtd.T         =   1;

  /*
   * Write async queue head into EHCI register space.
   */
  eh->opReg->asyncListBase.listAddr   =   (uint32_t)aqHeadPtr >> 5;
  eh->asyncHead       =   aqHeadPtr;

  /*
   * Perform initialization commands and start EHCI
   * controller.
   */
  eh->opReg->command.intThrCtrl = 0x08;
  eh->opReg->command.asSchEn = 1;
  eh->opReg->command.runStop = 1;

  /* 
   * Wait until reclamation bit is set to 0, indicating
   * empty asynchronous schedule detection
   */

  i = 0;
  while(eh->opReg->usbStat.recl) i++;

  eh->opReg->confFlagReg.confFlag = 1;

  lock_init((lock_t*)&eh->asq_lock);

  DEBUG("    EHCI Host controller initialized.");

  /* Initialise root hub */
  struct usb_hub_reg root_hub = {
    .name = hub_name,
    .hc = (void *)eh,
    .hc_ops = &ehci_hc_ops,
    .hub = (void *)eh,
    .hub_ops = &ehci_hub_ops,
    .port_count = eh->capReg->structPar.nPorts
  };

  uhub = usb_hub_init(&root_hub);

  if (uhub == NULL)
    return ERR_NO_MEM;

  hub_name[8]++;

  return 0;
}

