
#ifndef EHCI_H
#define EHCI_H

#include "../util.h"


struct hciVer{
    
    uint8_t minor;
    uint8_t major;

}__attribute__((packed));

struct hcsPar{

    uint32_t nPorts:4;
    uint32_t PPC:1;
    uint32_t res:2;
    uint32_t PRR:1;
    uint32_t NPCC:4;
    uint32_t NCC:4;
    uint32_t PIND:1;
    uint32_t res2:3;
    uint32_t dbgPrtN:4;
    uint32_t res3:8;
    
}__attribute__((packed));

struct hccPar{

    uint32_t addCap64:1;
    uint32_t prgFrmLstFlg:1;
    uint32_t asSchPC:1;
    const uint32_t res1:1;
    uint32_t isoSchThr:4;
    uint32_t EHCIExtCap:8;
    const uint32_t res2:16;
    
}__attribute__((packed));

struct hcspPrtRt{

    uint32_t res1;
    uint32_t res2;
    
}__attribute__((packed));


/* host controller capability registers */
struct hcCapReg{
    const uint8_t capLen;
    const uint8_t res;
    const struct hciVer version;
    const struct hcsPar structPar;
    const struct hccPar capabiPar;
    const struct hcspPrtRt portRoute; // Might not be resident
}__attribute__((packed));

struct hcUsbcmd{
  
    uint32_t runStop:1;
    uint32_t reset:1;
    uint32_t frLstSz:2;
    uint32_t prSchEn:1;
    uint32_t asSchEn:1;
    uint32_t intAsDB:1;
    uint32_t lHCR:1;
    uint32_t asSchPrkMdCnt:2;
    const uint32_t res1:1;
    uint32_t asSchPrkMdEn:1;
    const uint32_t res2:4;
    uint32_t intThrCtrl:8;
    const uint32_t res3:8;

}__attribute__((packed));

struct hcUsbSt{
  
    uint32_t usbInt:1;
    uint32_t usbErrInt:1;
    uint32_t prtChgDet:1;
    uint32_t frLstRoll:1;
    uint32_t hstSysErr:1;
    uint32_t intAsAdv:1;
    uint32_t res1:6;
    const uint32_t HCHalt:1;
    const uint32_t recl:1;
    const uint32_t perSchSt:1;
    const uint32_t asSchSt:1;
    const uint32_t res2:16;

}__attribute__((packed));

struct hcUsbIntr{

    uint32_t usbIntEn:1;
    uint32_t usbErrIntEn:1;
    uint32_t prtChgIntEn:1;
    uint32_t frmLstRollEn:1;
    uint32_t hostSysErrEn:1;
    uint32_t intAsyncAdvEn:1;
    const uint32_t res:26;

}__attribute__((packed));

struct hcFrInd{

    uint32_t frameInd:14;
    const uint32_t res:18;
    
}__attribute__((packed));

struct hcCtrlDsSeg{
    uint32_t offset;
}__attribute__((packed));

struct hcPerListBase{
  
    const uint32_t res:12;
    uint32_t baseAddr:20;

}__attribute__((packed));

struct hcAsyncLstBase{
  
    const uint32_t res:5;
    uint32_t listAddr:27;

}__attribute__((packed));

struct hcConfFlag{
    
    uint32_t confFlag:1;
    const uint32_t res:31;

}__attribute__((packed));

struct hcPortsc{
    
    const uint32_t curConSt:1;
    uint32_t conStChg:1;
    uint32_t portEn:1;
    uint32_t portEnDisChg:1;
    const uint32_t overCurAct:1;
    uint32_t overCurChg:1;
    uint32_t forcePrtRes:1;
    uint32_t suspend:1;
    uint32_t portRes:1;
    const uint32_t res1:1;
    const uint32_t lineStat:2;
    uint32_t portPower:1;
    uint32_t portOwn:1;
    uint32_t portIndCtrl:2;
    uint32_t portTestCtrl:4;
    uint32_t wakeConEn:1;
    uint32_t wakeDisEn:1;
    uint32_t wakeOvCurEn:1;
    const uint32_t res2:9;

}__attribute__((packed));



/* host controller operational registers */

struct hcOpReg{

    struct hcUsbcmd command;
    struct hcUsbSt usbStat;
    struct hcUsbIntr usbInt;
    struct hcFrInd frameInd;
    struct hcCtrlDsSeg segOff;
    struct hcPerListBase perListBase;
    struct hcAsyncLstBase asyncListBase;
    const uint32_t res[9];
    struct hcConfFlag confFlagReg;
    struct hcPortsc portsc[1];

    
}__attribute__((packed));


struct ehci{
    
    volatile struct hcOpReg *opReg;
    volatile struct hcCapReg *capReg;
    
    volatile struct aqhead *asyncHead;

    int next_address;

    struct pci_dev *pci;

    /* Queue Management */
    lock_t asq_lock;
};

int ehci_init(volatile struct ehci *eh);

#define EHCI_PTC_POWNER     1 << 13
#define EHCI_PTC_LINESTAT(port)         \
    (port >> 10) & 0x3
#define EHCI_PTC_PRESET     1 << 8
#define EHCI_PTC_SUSPEND    1 << 7
#define EHCI_PTC_PENCHG     1 << 3
#define EHCI_PTC_PENABLED   1 << 2
#define EHCI_PTC_CONNCHG    1 << 1
#define EHCI_PTC_CURCONSTAT 1

/* Some command defs */
#define EHCI_CMD_ASYNCEN    1 << 5
#define EHCI_CMD_PERSCEN    1 << 4
#define EHCI_CMD_RESET      1 << 1
#define EHCI_CMD_RUNSTP     1

/* USB interrupt register defs */
#define EHCI_INT_ASYNCADVEN         1 << 5
#define EHCI_INT_HOSTERREN          1 << 4
#define EHCI_INT_FRAMROLLEN         1 << 3
#define EHCI_INT_PORTCHNGEN         1 << 2
#define EHCI_INT_USBERREN           1 << 1
#define EHCI_INT_USBINTEN           1

#define EHCI_QH_RL_OFF              28
#define EHCI_QH_C_OFF               27
#define EHCI_QH_MAX_PACKET_LEN_OFF  16
#define EHCI_QH_H_OFF               15
#define EHCI_QH_DTC_OFF             14
#define EHCI_QH_EPS_OFF             12
#define EHCI_QH_ENDPT_OFF           8
#define EHCI_QH_I_OFF               7
#define EHCI_QH_ADDR_MASK       0x0000007F

#define EHCI_QH_MULT_OFF          30
#define EHCI_QH_PORT_NUM_OFF      23
#define EHCI_QH_HUB_ADDR_OFF      16
#define EHCI_QH_UFRAME_CMASK_OFF  8
#define EHCI_QH_UFRAME_SMASK_OFF  0

#define EHCI_TD_DATA_TOGGLE_OFF 31
#define EHCI_TD_TOTAL_SIZE_OFF  16
#define EHCI_TD_IOC_OFF         15
#define EHCI_TD_CERR_OFF        10
#define EHCI_TD_PID_OFF         8

#define EHCI_PID_OUT    0xE1
#define EHCI_PID_IN     0x69
#define EHCI_PID_SETUP  0x2D

#define EHCI_TD_STATUS_MASK     0x000000FF

#define EHCI_TD_BPOINTER_MASK   0xFFFFF000
#define EHCI_TD_POINTER_MASK    0xFFFFFFF0
#define EHCI_TD_TERMINATE       0x00000001


#define EHCI_STATUS_ACTIVE      0x80
#define EHCI_STATUS_HALTED      0x40
#define EHCI_STATUS_DATA_BUFFER 0x20
#define EHCI_STATUS_BABBLE      0x10
#define EHCI_STATUS_XACTERR     0x08
#define EHCI_STATUS_MISS_UF     0x04
#define EHCI_STATUS_SPLIT       0x02
#define EHCI_STATUS_PING        0x01




#endif
