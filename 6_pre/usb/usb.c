#include "../util.h"
#include "../sleep.h"
#include "uhci.h"
#include "pci.h"
#include "error.h"
#include "debug.h"
#include "allocator.h"
#include "list.h"

#include "usb_hid.h"
#include "usb_msd.h"
#include "usb_hub.h"

DEBUG_NAME("USB");

static struct list usb_drv_list_head;

/*
 * Builds and executes a control transfer 
 * Control transfers consists of up to three stages:
 *  1. SETUP
 *  2. IN/OUT
 *  3. STATUS 
 * It is always executed via the control pipe (pipe 0)
 */
#define USB_CREAD 1
#define USB_CWRITE 0
int control_transfer(struct usb_dev *udev,
                     struct usb_dev_setup_request *req, 
                     int dir, int  size, char *buff) {
  struct usb_pipe *pipe = &udev->control_pipe;
  int rc;

  /* Setup stage (reset toggle bit) */
  pipe->toggle_bit = 0;
  rc = udev->hc_ops->setup(pipe, req);
  if (rc != 8)
    return ERR_XFER;
  /* Read/write stage if necessary */
  if (size > 0) {
    if (dir == USB_CREAD) 
      rc = udev->hc_ops->read(pipe, size, buff);
    else
      rc = udev->hc_ops->write(pipe, size, buff);
 
    if (rc != size)
      return ERR_XFER;
  }
  /* Status stage (always uses toggle bit 1) */
  pipe->toggle_bit = 1;
  if (dir == USB_CREAD)
    rc = udev->hc_ops->write(pipe, 0, NULL);
  else
    rc = udev->hc_ops->read(pipe, 0, NULL);

  if (rc != 0) 
    return ERR_XFER;

  return 0;
}

/*
 * Reads a descriptor from USB device to a buffer
 * Executed over a USB device control pipe 
 */
int load_descriptor(struct usb_dev *udev, int desc_type, 
                    int desc_index, int size, char *buff) {
  int rc;

  struct usb_dev_setup_request get_descriptor_req = {
    .bmRequestType = 0x80,                  /* direction device-host */
    .bRequest = GET_DESCRIPTOR,
    .wValue = (desc_type << 8) | desc_index,
    .wIndex = 0,
    .wLength = size
  };

  rc = control_transfer(udev, &get_descriptor_req, USB_CREAD,
                        size, buff);

  return rc;
}

/*
 * Reads device configuration with index i to buffer 
 */
int usb_load_configuration(struct usb_dev *udev, 
                              int conf_index,
                              char *buff) {
  struct usb_dev_configuration udc;
  int size;
  int rc;

  size = sizeof(struct usb_dev_configuration); 
  rc = load_descriptor(udev, USB_CONFIGURATION_DESCRIPTOR,
                       conf_index, size, (char *)&udc);

  if (rc < 0)
    return ERR_XFER;
  
  if (udc.bDescriptorType != USB_CONFIGURATION_DESCRIPTOR)
    return ERR_XFER;

  rc = load_descriptor(udev, USB_CONFIGURATION_DESCRIPTOR, 
                       conf_index, udc.wTotalLength, buff);

  if (rc == 0)
    return udc.wTotalLength;

  return ERR_XFER;
}

/*
 * Finds descriptor of a given type  
 *
 * Updates the remaining buffer size and pointer
 */
int usb_get_next_descriptor(int descriptor_type, 
                        int *left, char **buff) {

  struct usb_generic_header *ugh; 

  ugh = (struct usb_generic_header *)*buff;

  do {
    /* As the first step advance to the next descriptor */
    *buff += ugh->bLength;
    *left -= ugh->bLength;

    /* If no more descriptors in this buffer */
    if (*left <= 0) 
      return -1;

    ugh = (struct usb_generic_header *)*buff;
    /* Descriptor type matches? */
  } while (ugh->bDescriptorType != descriptor_type);
  
  /* Return this descriptor */
  return 0;
}

/*
 * Transmits SETUP request to USB device 
 */
static int send_request(struct usb_pipe *pipe, uint8_t rec, 
                       uint8_t req_type, uint16_t value, 
                       uint16_t index) {
  struct usb_dev *udev = pipe->udev;

  struct usb_dev_setup_request req = {
    .bmRequestType = 0 | rec,  /* direction host-device, recipient*/
    .bRequest = req_type,
    .wValue = value,
    .wIndex = index,
    .wLength = 0
  };

  return control_transfer(udev, &req, USB_CWRITE, 0, NULL);
}


static int usb_lookup_driver(struct usb_interface *uifc) {
  struct usb_driver_list *udl;

  LIST_FOR_EACH(&usb_drv_list_head, udl)
    if (uifc->class_code == udl->udd->class_code) {
      /* 
       * We do not initialise the USB device driver before 
       * USB device was not initialised completely
       */
      uifc->udd = udl->udd;
      return 0;
    }

  return ERR_NO_DRIVER; 
}

/*
 * Helper function, configures an interface 
 */
static int usb_configure_interface(struct usb_dev *udev, 
                                   int rm_buff, char *ptr) {
  struct usb_conf_interface *conf_ifc;
  struct usb_ifc_endpoint *ifc_ep;
  struct usb_interface *uifc;
  struct usb_pipe *pipe, *pipe_h;

  uifc = kzalloc(sizeof(struct usb_interface));

  if (uifc == NULL)
    return ERR_NO_MEM;

  conf_ifc = (struct usb_conf_interface *)ptr;

  /* Copy the interface information */
  uifc->class_code = conf_ifc->bInterfaceClass; 
  uifc->subclass_code = conf_ifc->bInterfaceSubClass; 
  uifc->protocol_code = conf_ifc->bInterfaceProtocol;
  uifc->number = conf_ifc->bInterfaceNumber;

  DEBUG("  class: %02x, subclass: %02x",
      uifc->class_code, uifc->subclass_code);
  DEBUG("  endpoints: %d", conf_ifc->bNumEndpoints);
  DEBUG("  alternative: %d", conf_ifc->bAlternateSetting);

  if (usb_lookup_driver(uifc) < 0) {
    DEBUG("Driver not found");

    kfree(uifc);

    return ERR_NO_DRIVER;
  }

  DEBUG("Driver found");

  uifc->udev = udev;

  /* Read all pipes related to this configuration */
  LIST_INIT(&uifc->pipes);

  while (usb_get_next_descriptor(USB_ENDPOINT_DESCRIPTOR, 
                                 &rm_buff, &ptr) == 0) {
    ifc_ep = (struct usb_ifc_endpoint *)ptr;
    pipe = kzalloc(sizeof(struct usb_pipe));

    if (pipe == NULL) {
      /* Error, deallocated previously allocated pipes */
      LIST_FOR_EACH_SAFE(&uifc->pipes, pipe, pipe_h)
        kfree(pipe);

      return ERR_NO_MEM;
    }

    pipe->udev = udev;
    pipe->ep_address = ifc_ep->bEndpointAddress & 0x0f;
    pipe->direction = (ifc_ep->bEndpointAddress & USB_PIPE_DIR_MASK) != 0 ?
      USB_PIPE_DIR_IN : USB_PIPE_DIR_OUT;
    pipe->attributes = ifc_ep->bmAttributes;
    pipe->max_packet_size = ifc_ep->wMaxPacketSize;
    pipe->interval = ifc_ep->bInterval;

    LIST_LINK(&uifc->pipes, pipe);
  }

  /* Link this interface onto the device interface list */
  LIST_LINK(&udev->interfaces, uifc);

  return 0;
}

/*
 * This function configures USB device, this involves:
 *  - assigning configuration 
 *  - assigning unique address 
 *  - attaching USB device driver
 * Function assumes that there is only one device 
 * responding to address 0 on this host controller.
 */
int usb_configure_device(struct usb_dev *udev, int address) {
  /* Configuration and interface iterators */
  struct usb_dev_descriptor *udev_desc = NULL;
  struct usb_dev_configuration *udev_conf = NULL;
  struct usb_interface *uifc, *uifc_h;
  struct usb_pipe *pipe, *pipe_h;
  char buff[512];
  char *ptr;
  int conf_size = 0;
  int conf_count;
  int conf_i;
  int conf_ok; 
  int rc;
  int at_least_one;

  /* 
   * Top level configuration of the USB device: 
   *  1. Setup address 0 
   *  2. Setup control pipe 
   *  3. Read device descriptor
   */
  DEBUG("Configuring USB device");

  /* After reset all devices are reachable via address 0 */
  udev->address = 0;

  /* Setup control pipe (pipe 0) */
  bzero((char *)&udev->control_pipe, sizeof(struct usb_pipe));
  udev->control_pipe.udev = udev;
  /*
   * Default max packet size in the control pipe are:
   *  8B for LS and 64B for FS and HS devices 
   */
  udev->control_pipe.max_packet_size = 
    udev->usb_sc == USB_LS_DEV ? 8 : 64;   

  /* Function loads device descriptor to buff */
  rc = load_descriptor(udev, USB_DEVICE_DESCRIPTOR, 0, 
                       sizeof(struct usb_dev_descriptor), buff);
  /* device descriptor index is always 0 */

  DEBUG("reading device descriptor\t%s", DEBUG_STATUS(rc));
  if (rc < 0)
    return ERR_DEV_STALLED;

  udev_desc = (struct usb_dev_descriptor *)buff;

  /* Update the maximum packet size for the control pipe */
  udev->control_pipe.max_packet_size = udev_desc->bMaxPacketSize0;

  conf_count = udev_desc->bNumConfigurations;

  /* 
   * Device has n configurations (usually only one), we scan 
   * through these configurations and choose one. We choose 
   * the configuration that can be handled by register drivers.
   */
  LIST_INIT(&udev->interfaces);
  conf_i = 0;
  conf_ok = 0;
  do {
    DEBUG("Reading device configuration %d", conf_i);
    /* 
     * Function loads the complete device configuration with interfaces,
     * endpoints descriptors and strings to buff. 
     */
    conf_size = usb_load_configuration(udev, conf_i, buff);
    udev_conf = (struct usb_dev_configuration *)buff;

    if (conf_size < 0) {
      DEBUG(".. failed to read the configuration");
      return ERR_DEV_STALLED;
    }

    DEBUG("Number of interfaces: %d, reading..", udev_conf->bNumInterfaces);

    /* Scan this configuration interfaces */
    ptr = (char *)buff;
    while (usb_get_next_descriptor(USB_INTERFACE_DESCRIPTOR, 
                                   &conf_size, &ptr) == 0) {

      if (usb_configure_interface(udev, conf_size, ptr) == 0) {
        /* 
         * This configuration has at least one interface 
         * that is supported. Therefore we choose this 
         * configuration 
         */
        conf_ok = 1;
        udev->configuration_id = udev_conf->bConfigurationValue;
        udev->configuration_index = conf_i;
      }
    }
  } while ((conf_ok == 0) && (++conf_i < conf_count));

  /* If not configuration is supported */
  if (conf_ok == 0) {
    /* No structure has been allocated, we can return */
    return ERR_NO_DRIVER;
  }

  /* Configure address  */
  rc = send_request(&udev->control_pipe, USB_RECIV_DEVICE,
                    SET_ADDRESS, (uint16_t)address, 0);
  DEBUG("Setting up address %d\t%s", address, DEBUG_STATUS(rc));
  udev->address = address;
/*
 * Bochs returns error while configuring device address therefore
 * we comment it out for now
 */
/*
  if (rc < 0)
    goto init_fail;
*/
  
  /* Choose configuration */
  rc = send_request(&udev->control_pipe, USB_RECIV_DEVICE,
                   SET_CONFIGURATION, udev->configuration_id, 0);
  DEBUG("Choosing configuration %d\t%s",
       udev->configuration_id, DEBUG_STATUS(rc));

  if (rc < 0)
    goto usb_configure_fail;
 
  udev->op_state = USB_STATUS_CONFIGURED;

  /* Initilise device drivers of the registered interfaces */
  at_least_one = 0;
  LIST_FOR_EACH(&udev->interfaces, uifc)
    if (uifc->udd->init(uifc) == 0)
      at_least_one = 1;
  
  if (at_least_one == 0)
    return ERR_NO_DRIVER;

  return 0;

usb_configure_fail:
  /* Deallocate resources and report fail */
  LIST_FOR_EACH_SAFE(&udev->interfaces, uifc, uifc_h) {
    LIST_FOR_EACH_SAFE(&uifc->pipes, pipe, pipe_h)
      kfree(pipe);
    kfree(uifc);
  }

  return ERR_DEV_STALLED;
}

void usb_free_device(struct usb_dev *udev) {
  struct usb_interface *uifc, *uifc_h;
  struct usb_pipe *pipe, *pipe_h;

  LIST_FOR_EACH_SAFE(&udev->interfaces, uifc, uifc_h) {
    /* Call interface driver rutine */
    uifc->udd->free(uifc);

    /* Free all pipes related to this interface */
    LIST_FOR_EACH_SAFE(&uifc->pipes, pipe, pipe_h)
      kfree(pipe);

    /* Release the interface */
    kfree(uifc);
  }
}

/*
 * Functions for USB device drivers 
 */
int usb_read(struct usb_pipe *pipe, int size, char *data) {
  struct usb_dev *udev = pipe->udev;

  return udev->hc_ops->read(pipe, size, data);
}

int usb_write(struct usb_pipe *pipe, int size, char *data) {
  struct usb_dev *udev = pipe->udev;

  return udev->hc_ops->write(pipe, size, data);
}

int usb_setup(struct usb_dev *udev, struct usb_dev_setup_request *req,
              int dir, int size, char *data) {
  return control_transfer(udev, req, dir, size, data);
}

/*
 * Used to register USB drivers 
 */
int usb_dev_driver_register(struct usb_dev_driver *udd) {
  struct usb_driver_list *udl;

  udl = kzalloc(sizeof(struct usb_driver_list));
  if(udl == NULL)
    return ERR_NO_MEM;

  udl->udd = udd;

  LIST_LINK(&usb_drv_list_head, udl);

  return 0;
}

int usb_static_init() {
  LIST_INIT(&usb_drv_list_head);

  usb_hub_static_init();
  usb_msd_static_init();
  usb_hid_static_init();
  uhci_static_init();
  pci_static_init();

  return 0;
}

