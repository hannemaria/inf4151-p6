#ifndef ERROR_H
#define ERROR_H

#define SUCCESS  0
#define ERR_NO_MEM -1
#define ERR_DEV_STALLED -2
#define ERR_XFER -3
#define ERR_PROTO -4          /* Protocol violated */
#define ERR_DEV_MAP -5        /* Failed to map driver register */
#define ERR_NO_DRIVER -1000

#endif
