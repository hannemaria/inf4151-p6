#ifndef DEBUG_H
#define DEBUG_H

#include "../console.h"

#define DEBUG_MODE 1
#define CONSOLE_PRINT 1

#if DEBUG_MODE
    #define DEBUG_NAME(x) \
        static const char debug_name[]=x; 

    #define DEBUG_STATUS(x) (((x) >= 0) ? "OK" : "ERR")

    #if CONSOLE_PRINT

        struct console *debug_con;

         #define DEBUG(args...)  { \
            cprintf(debug_con, "%8s ", debug_name); \
            cprintf(debug_con, args); \
            cprintf(debug_con, "\n"); \
            cdraw(debug_con); \
        }

    #else
        #define DEBUG(args...)  { \
            rsprintf("%8s ", debug_name); \
            rsprintf(args); \
            rsprintf("\r\n"); \
            ms_delay(100); \
        }
    #endif // CONSOLE_PRINT
#else
  #define DEBUG_NAME(x)
  #define DEBUG(...)
#endif  // DEBUG_MODE


#endif  // DEBUG_H

 //     cdraw(debug_con); 
