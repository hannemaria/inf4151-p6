#ifndef USB_H
#define USB_H

#include "../util.h"
#include "list.h"

void usb_mass_storage_init();

/*
 * USB device speed classes
 */
enum usb_speed_class_e {
  USB_LS_DEV = 0,
  USB_FS_DEV = 1,
  USB_HS_DEV = 2
};

typedef enum usb_speed_class_e usb_speed_class;

/*
 * USB device states 
 */
enum usb_device_state_e {
  USB_STATUS_ATTACHED,
  USB_STATUS_POWERED,
  USB_STATUS_DEFAULT,
  USB_STATUS_ADDRESS,
  USB_STATUS_CONFIGURED,
  USB_STATUS_SUSPENDED
};

typedef enum usb_device_state_e usb_device_state;

struct usb_dev;
struct usb_hc_ops;
struct usb_dev_driver;

/*
 * USB device interface
 */
struct usb_interface {
  struct list list;       /* Hook to link the interfaces */
  struct usb_dev *udev;   /* USB device owning this interface */

  /* Interface characteristic */
  uint8_t class_code;
  uint8_t subclass_code;
  uint8_t protocol_code;

  uint8_t number;         /* Interface number in this configuration */
  struct list pipes;      /* Interfaces communication pipes */

  /* Interface device driver */
  struct usb_dev_driver *udd;
  void *driver_data;
};

struct usb_pipe {
  struct list list;
  struct usb_dev *udev;      /* USB device that owns this pipe */
  struct usb_interface *uifc;/* USB interface that this pipe belongs to */

  uint8_t ep_address;        /* Pipe endpoint address */
  uint8_t direction;         /* Pipe traffic direction */
  uint8_t attributes;        /* Pipe attributes like: transfer, synch, and usage type */
  uint16_t max_packet_size;  /* Maximum packet size for this endpoint */
  uint8_t interval;          /* Interval for polling endpoint for data transfers */ 
  int toggle_bit;            /* Used for USB transmission synchronisation */
  void *xfer;                /* Transfer related to this pipe, using void
                              * type since there may be various transfer 
                              * types depending on host controller
                              * implementation 
                              */
};

#define USB_PIPE_DATA_BULK 0x02
#define USB_PIPE_INTERRUPT 0x03
#define USB_PIPE_DIR_MASK 0x80
#define USB_PIPE_DIR_IN 0x01
#define USB_PIPE_DIR_OUT 0x00

/* 
 * USB device structure 
 */
struct usb_dev {
  /* USB device attachment */
  struct usb_hub *hub;
  struct usb_hub_port *port;   /* Device port ID on a give USB hub */

  /* Host controller interface */
  struct usb_hc_ops *hc_ops;
  void *hc;
  uint8_t address;           /* Logical address of this USB device */

  usb_speed_class usb_sc;    /* USB device speed                  */
  usb_device_state op_state; /* the current USB device state */

  /* Current configuration */
  uint8_t configuration_id;
  uint8_t configuration_index;
  /* Interfaces present in this configuration */
  struct list interfaces;

  /* Control pipe */
  struct usb_pipe control_pipe;
};

/* 
 * Host controller operations 
 */
struct usb_dev_setup_request;
struct usb_interrupt;
struct usb_hc_ops {
  int (*read)(struct usb_pipe *, int size, char *data);
  int (*write)(struct usb_pipe *, int size, char *data);
  int (*setup)(struct usb_pipe *, struct usb_dev_setup_request *data);
  uint8_t (*get_next_addr)(struct usb_dev *);
  int (*register_interrupt_h)(struct usb_interrupt *);
  int (*remove_interrupt_h)(struct usb_interrupt *);
};

/*
 * Device driver struct and interface to register 
 */

struct usb_dev_driver {
  uint8_t class_code;
  int (*init)(struct usb_interface *);
  void (*free)(struct usb_interface *);
};

struct usb_driver_list {
  struct list list;
  struct usb_dev_driver *udd;
};

/*
 * Function prototypes for USB device drivers 
 */
int usb_read(struct usb_pipe *pipe, int size, char *data);
int usb_write(struct usb_pipe *pipe, int size, char *data);
/* Control direction */
#define USB_CREAD 1
#define USB_CWRITE 0
int usb_setup(struct usb_dev *udev, struct usb_dev_setup_request *req,
              int dir, int size, char *data);

/* 
 * USB interrupts 
 */
struct usb_interrupt {
  struct usb_pipe *pipe;
  char *buff;
  int size;
  int freq;
  /* Interrupt handler related data */
  void (*interrupt_h)(void *data);
  void *data;
};

/* Initialises the USB subsystem */
int usb_static_init();
/* Registers the USB device driver */
int usb_dev_driver_register(struct usb_dev_driver *udd);
/* 
 * Configures a single USB device by: 
 *  1. Reading the device descriptor
 *  2. Matching a device driver to the device
 *  3. Registering all device pipes
 *  4. Setting up device address and configuration 
 */
int usb_configure_device(struct usb_dev *, int );
/*
 * When a device is unplugged usb_free_device()
 * releases resources associated with the device
 */
void usb_free_device(struct usb_dev *);

/* Loads USB device configuration into the given buffer */
int usb_load_configuration(struct usb_dev *udev, 
                           int conf_index, char *buff);

/* 
 * Searches for the next descriptor of the given type
 * Parameters: 
 *  left - remaining bytes of the device configuration 
 *  buff - pointer to the configuration buffer
 * Remark: Both variables are updated
 *
 * Return 0 if operation succeeded else -1
 */
int usb_get_next_descriptor(int descriptor_type, 
                            int *left, char **buff);

enum bRequest_e {
  GET_STATUS = 0,
  CLEAR_FEATURE = 1,
  SET_FEATURE = 3,
  SET_ADDRESS = 5,
  GET_DESCRIPTOR = 6,
  SET_DESCRIPTOR = 7,
  GET_CONFIGURATION = 8,
  SET_CONFIGURATION = 9,
  GET_INTERFACE = 10,
  SET_INTERFACE = 11,
  SYNCH_FRAME = 12
};

#define USB_RECIV_DEVICE 0
#define USB_RECIV_INTERFACE 1
#define USB_RECIV_ENDPOINT 2

/* Feature selectors */
#define USB_FEATURE_ENDPOINT_HALT 0
#define USB_FEATURE_DEVICE_REMOTE_WAKEUP 1
#define USB_FEATURE_TEST_MODE 2

/* 
 * USB descriptor request types and helper functions
 *
 * Note: USB device must support only requests 
 * for the first three types of descriptors
 */

/* 
 * Loads the size bytes of the device descriptor of
 * the given type and index into the given buffer
 *
 * It is mandatory that a device returns the device descriptor
 * and the configuration descriptor in respons to this request.
 * The remaining descriptors may not be return. In order to 
 * obtain them, it is necessary to first read the complete 
 * configuration into a buffer (using load_configuration()) 
 * and then look for a specific configuration in a buffer 
 * (using get_descriptor_ptr())
 */
int load_descriptor(struct usb_dev *udev, int desc_type,
                    int desc_index, int size, char *buff);
/* 
 * Loads the complete device configuration into the given
 * buffer 
 */
int load_configuration(struct usb_dev *udev, int conf_index,
                       char *buff);
/*
 * Returns a pointer to the descriptor of the given type 
 * and index that is found in the buff 
 */
char *get_descriptor_ptr(int descriptor_type, int index,
                         int conf_size, char *buff);

#define USB_DEVICE_DESCRIPTOR  0x01
#define USB_CONFIGURATION_DESCRIPTOR 0x02
#define USB_STRING_DESCRIPTOR 0x03
#define USB_INTERFACE_DESCRIPTOR 0x04
#define USB_ENDPOINT_DESCRIPTOR 0x05

struct usb_configuration_header {
  uint8_t bLength;
  uint8_t bDescriptorType;
  uint16_t wTotalLength;
} __attribute__((packed));

struct usb_generic_header {
  uint8_t bLength;
  uint8_t bDescriptorType;
} __attribute__((packed));

struct usb_dev_setup_request {
  uint8_t bmRequestType;
  uint8_t bRequest;
  uint16_t wValue;
  uint16_t wIndex;
  uint16_t wLength;
} __attribute__((packed));

struct usb_dev_descriptor {
  uint8_t bLength;
  uint8_t bDescriptorType;
  uint16_t bcdUSB;
  uint8_t bDeviceClass;
  uint8_t bDeviceSubClass;
  uint8_t bDeviceProtocol;
  uint8_t bMaxPacketSize0;
  uint16_t idVendor;
  uint16_t idProduct;
  uint16_t bcdDevice;
  uint8_t iManufacturer;
  uint8_t iProduct;
  uint8_t iSerialNumber;
  uint8_t bNumConfigurations;
} __attribute__((packed));

struct usb_dev_configuration {
  uint8_t bLength;
  uint8_t bDescriptorType;
  uint16_t wTotalLength;
  uint8_t bNumInterfaces;
  uint8_t bConfigurationValue;
  uint8_t iConfiguration;
  uint8_t bmAttributes;
  uint8_t bMaxPower;
} __attribute__((packed));

struct usb_conf_interface {
  uint8_t bLength;
  uint8_t bDescriptorType;
  uint8_t bInterfaceNumber;
  uint8_t bAlternateSetting;
  uint8_t bNumEndpoints;
  uint8_t bInterfaceClass;
  uint8_t bInterfaceSubClass;
  uint8_t bInterfaceProtocol;
  uint8_t iInterface;
} __attribute__((packed));

struct usb_ifc_endpoint {
  uint8_t bLength;
  uint8_t bDescriptorType;
  uint8_t bEndpointAddress;
  uint8_t bmAttributes;
  uint16_t wMaxPacketSize;
  uint8_t bInterval;
} __attribute__((packed));

#endif
