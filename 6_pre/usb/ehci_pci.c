
#include "allocator.h"
#include "pci.h"
#include "error.h"
#include "ehci.h"
#include "ehci_pci.h"
#include "debug.h"
#include "../util.h"
#include "../memory.h"

DEBUG_NAME("EHCI PCI");

/*
 * 
 */
int ehci_pci_init(struct pci_dev *pci) {

    uint32_t iUsbbase;
    volatile struct ehci *eh;
    struct ep_usbbase *usbbase;

    DEBUG("Found EHCI controler");

    eh = kzalloc(sizeof(struct ehci));
    if(eh == NULL){
        DEBUG("Out of memory in ehci_pci_init");
        return ERR_NO_MEM;
    }


    iUsbbase    =   (uint32_t)pci_read_dev_reg32(pci, EHCI_PCIREG_BASE);
    usbbase     =   (struct ep_usbbase*) &iUsbbase;

    DEBUG("    USBBASE");
    DEBUG("    EHCI register base address: %x", usbbase->bAddr);
    DEBUG("    Type: %s", usbbase->type == 0 ? "Map32" : "Map64");

    if(identity_map(usbbase->bAddr << 8, 152)){
      DEBUG("Failed to identity map driver register.");
      return ERR_DEV_MAP;
    }

    eh->pci = pci;

    pci->driver = (void*) eh;

    return ehci_init(eh);
}

/* ?? */
void ehci_pci_interrupt(void* driver){

  return;
}

/* EHCI PCI interface */
static struct pci_dev_driver ehci_pci_driver = {
    .class_code = 0x0c,
    .subclass_code = 0x03,
    .prog_ifc = 0x20,
    .init = ehci_pci_init,
    .interrupt = ehci_pci_interrupt,
};

void ehci_pci_dev_driver_register() {
    pci_dev_driver_register(&ehci_pci_driver);
}


