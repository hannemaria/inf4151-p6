

#ifndef ASYNC_QUEUE
#define ASYNC_QUEUE

#include "../util.h"

struct qtdPtr{

    uint32_t T:1;
    const uint32_t res:4;
    uint32_t nextQtdPtr:27;

}__attribute__((packed));

struct qtdPtrAlt{

    uint32_t T:1;
    uint32_t nakCnt:4;
    uint32_t nextQtdPtr:27;

}__attribute__((packed));

struct bufPtrOff{

    uint32_t curOffset:12;
    uint32_t bufPtrPage:20;
    
}__attribute__((packed));

struct bufPtr{

    uint32_t res:12;
    uint32_t bufPtrPage:20;
    
}__attribute__((packed));

struct extBufPtr{
    uint32_t upperAddr;
}__attribute__((packed));


struct aqtd{

    /* First byte */
    struct qtdPtr nextQtd;

    /* Second byte */
    struct qtdPtrAlt nextAltQtd;

    /* Third byte */
    uint32_t status:8;
    uint32_t pidCode:2;
    uint32_t cerr:2;
    uint32_t c_page:3;
    uint32_t ioc:1;
    uint32_t totTransLen:15;
    uint32_t dt:1;

    /* Fourth -> eight */
    struct bufPtrOff bufferPtrOff;

    struct bufPtr bufferPtr[4];

    /* Extended pointers for 64 bit compatibility */
    struct extBufPtr extBufPtr[5];

}__attribute__((packed));

struct aqhead{
    /* First 32 bit field */
    uint32_t T:1;
    uint32_t llTyp:2;
    const uint32_t res1:2;
    uint32_t hPointer:27;

    /* Second 32 bit field */
    uint32_t devAddr:7;
    uint32_t I:1;
    uint32_t endPt:4;
    uint32_t eps:2;
    uint32_t dtc:1;
    uint32_t H:1;
    uint32_t maxPcktLen:11;
    uint32_t C:1;
    uint32_t RL:4;

    /* Third bit field */
    uint32_t Smask:8;
    uint32_t Cmask:8;
    uint32_t hubAddr:7;
    uint32_t portNum:7;
    uint32_t mult:2;

    /* Fourth bit field */
    const uint32_t res2:5;
    uint32_t curQtdPtr:27;

    /* Queue Transmission Descriptor */
    struct aqtd qtd;

}__attribute__((packed));

#define ASYNCQ_LLTYP_QHEAD 1

#define QTD_STATUS_ACTIVE       1 << 7
#define QTD_STATUS_HALTED       1 << 6
#define QTD_STATUS_DBERR        1 << 5
#define QTD_STATUS_BABBLE       1 << 4
#define QTD_STATUS_TRERR        1 << 3
#define QTD_STATUS_MISSMF       1 << 2
#define QTD_STATUS_STS          1 << 1
#define QTD_STATUS_PINGST       1

#define EHCI_QTD_MAXTRANS_SIZE 20480

#endif




