enum {
    FSE_OK = 0,
    /* Unspecified error */
    FSE_ERROR = -1,
    /* File system is inconsistent */
    FSE_FSINC = -2,
    /* Invalid mode */
    FSE_INVALIDMODE = -3,
    /* Filename is to long */
    FSE_NAMETOLONG = -4,
    /* File does not exist */
    FSE_NOTEXIST = -5,
    /* Invalid file handle */
    FSE_INVALIDHANDLE = -6,
    /* Invalid offset */
    FSE_INVALIDOFFSET = -7,
    /* End of file reached */
    FSE_EOF = -8,
    /* File already exist */
    FSE_EXIST = -9,
    /* Invalid filename */
    FSE_INVALIDNAME = -10,
    /* Directory is not empty */
    FSE_DNOTEMPTY = -11,
    /* Invalid inode */
    FSE_INVALIDINODE = -12,
    /* Out of file descriptor table entrys */
    FSE_NOMOREFDTE = -13,
    /* Bitmap error */
    FSE_BITMAP = -14,
    /* Out of inodes */
    FSE_NOMOREINODES = -15,
    /* Out of datablocks */
    FSE_FULL = -16,
    /* Could not add directory entry */
    FSE_ADDDIR = -17,
    /* Direcory entry was not found */
    FSE_DENOTFOUND = -18,
    /* A given directory path is actually a file */
    FSE_DIRISFILE = -19,
    /* Parse filename error */
    FSE_PARSEERROR = -20,
    /* Inode table full */
    FSE_INODETABLEFULL = -21,
    /* Invalid block */
    FSE_INVALIDBLOCK = -22,
    /* Tried to delete a file that was opened by another program */
    FSE_FILEOPEN = -23,
    /* To many open files */
    FSE_TMOPEN = -24,
    /* Not correct access privileges */
    FSE_NACCESS = -25,
    /* File is not open */
    FSE_NOPEN = -26,
    /* Invalid index */
    FSE_INVALIDINDEX = -27,
    /* Invalid whence */
    FSE_INVALIDWHENCE = -28,
    /* Cannot link to a directory */
    FSE_LINKDIR = -29,
    /* Cannot unlink a directory */
    FSE_ULINKDIR = -30,
    /* Must be a directory */
    FSE_DIRECTORY = -31,
    /* Is already open */
    FSE_ALREADYOPEN = -32,
    /* File is open */
    FSE_OPEN = -33,

    /* END OF ERROR MESSAGES */
    FSE_COUNT = -34
};

enum {
    MAX_ERROR_LENGTH = 50
};

struct fse {
    int error;
    char msg[MAX_ERROR_LENGTH];
};

void print_fse(int error);


/*
 * File system error codes, and a function to print them.
 */


struct fse et[] = {
  //  {FSE_MAX, "123456789012345678901234567890"},
  {FSE_OK, "No error"},
  {FSE_ERROR, "Unspecified"},
  /* File system is inconsistent */
  {FSE_FSINC, "File system is inconsistent" },
  /* Invalid mode */
  {FSE_INVALIDMODE, "Invalid mode"},
  /* Filename is to long */
  {FSE_NAMETOLONG, "Filename is to long"},
  /* File does not exist */
  {FSE_NOTEXIST, "File does not exist"},
  /* Invalid file handle */
  {FSE_INVALIDHANDLE, "Invalid file handle"},
  /* Invalid offset */
  {FSE_INVALIDOFFSET, "Invalid offset"},
  /* End of file reached */
  {FSE_EOF, "End of file"},
  /* File already exist */
  {FSE_EXIST, "A file with that name already exist"},
  /* Invalid filename */
  {FSE_INVALIDNAME, "Invalid filename"},
  /* Directory is not empty */
  {FSE_DNOTEMPTY, "Directory is not empty"},
  /* Invalid inode */
  {FSE_INVALIDINODE, "Inode error"},
  /* Out of file descriptor table entrys */
  {FSE_NOMOREFDTE, "Out of file descriptor table entries"},
  /* Bitmap error */
  {FSE_BITMAP, "Bitmap error"},
  /* Out of inodes */
  {FSE_NOMOREINODES, "Out of inodes"},
  /* Out of datablocks */
  {FSE_FULL, "Out of data blocks"},
  /* Could not add directory entry */
  {FSE_ADDDIR, "Could not add directory entry"},
  /* Direcory entry was not found */
  {FSE_DENOTFOUND, "Directory entry not found"},
  /* A given direcrtory path is actually a file */
  {FSE_DIRISFILE, "Directory is a file"},
  /* Parse filename error */
  {FSE_PARSEERROR, "Parse filename error"},
  /* Inode table full */
  {FSE_INODETABLEFULL, "Inode table full"},
  /* Invalid block */
  {FSE_INVALIDBLOCK, "Inode contains invalid block pointer"},
  /* Tried to delete a file that was opened by another program */
  {FSE_FILEOPEN, "File to delete is used by another program"},
  /* To many open files */
  {FSE_TMOPEN, "To many open files in memory"},
  /* Not correct access privileges */
  {FSE_NACCESS, "Not enough privileges to access"},
  /* File is not open */
  {FSE_NOPEN, "File is not open by this process"},
  /* Invalid index */
  {FSE_INVALIDINDEX, "Not valid index"},
  /* Invalid whence in fs_lseek */
  {FSE_INVALIDWHENCE, "Not valid whence (not any of SET, CUR, END)"},
  /* Cannot link to a directory */
  {FSE_LINKDIR, "Can not link a directory"},
  /* Cannot unlink a directory */
  {FSE_ULINKDIR, "Can not unlink a directory"},
  /* Must be a directory */
  {FSE_DIRECTORY, "Must be a directory"},
  /* The file/directory is already open */
  {FSE_ALREADYOPEN, "The file/directory is already open"},
  /* File is open */
  {FSE_OPEN, "File is open"}
};


#define FSE_LINE 0
#define FSE_COL 0

void print_fse(int error)
{
  if (error >= 0) {
    scrprintf(FSE_LINE, FSE_COL, et[0].msg);
  } else if (error > FSE_COUNT) {
    scrprintf(FSE_LINE, FSE_COL, et[error * -1].msg);
  } else {
    scrprintf(FSE_LINE, FSE_COL, "Invalid error number\n");
  }
}
