#include "util.h"
#include "console.h"
#include "print.h"

static inline char *get_cursor_ptr(struct console *con) {
    char *c;

    c = con->buffer + (con->current_top + con->cursor) % con->total_size;

    return c;
}

static void _scroll(struct console *con) {
    int i;
    char *c;

    if (con->cursor < con->size)
        return;

    con->cursor -= con->width;
    con->current_top += con->width;
    con->current_top %= con->total_size;

    /* Clear new line */
    c = get_cursor_ptr(con);
    for (i = 0; i < con->width; i++, *c++ = '\0');
}

static int put_char(struct console *con, char c) {
    char *p;

    p = get_cursor_ptr(con);
    *p = c;

    con->cursor++;
    _scroll(con);

    return 1;
}

static int new_line(struct console *con) {
    int to_end; 

    to_end = con->width - (con->cursor % con->width);
    con->cursor += to_end;

    _scroll(con);

    return 1;
}

/* Move to the next tabulator stop */
static int tab(struct console *con) {
    int to_next_tab;
    int cursor_x;
    int count = 0;

    cursor_x = con->cursor % con->width;
    to_next_tab = con->tab_stop - (cursor_x % con->tab_stop);
    /* 
     * Do no wrap to next line when tabulating instead 
     * stop at the new line 
     */
    to_next_tab = to_next_tab + cursor_x < con->width ?
        to_next_tab : con->width - cursor_x;

    while (to_next_tab-- > 0)
        count += put_char(con, ' ');

    return count;
}

/* 
 * Move cursor back
 */
static int cursor_back(struct console *con) {
    if (con->cursor <= 0)
        return 0;


    con->cursor--;

    if (con->esc_number_count == 1)
        if (con->esc_num[0]-- > 0)
            cursor_back(con);

    return 1;
}

/*
 * Move cursor forward
 */
static int cursor_forward(struct console *con) {
    if (con->cursor >= con->size)
        return 0;

    if (con->esc_number_count == 1)
        if (con->esc_num[0]-- > 0)
            cursor_forward(con);

    return 1;
}

static int cursor_up(struct console *con) {
    if (con->cursor - con->width < 0)
        return 0;

    con->cursor -= con->width;

    if (con->esc_number_count == 1)
        if (con->esc_num[0]-- > 0)
            cursor_up(con);

    return 1;
}

static int cursor_down(struct console *con) {
    if (con->cursor + con->width >= con->size)
        return 0;

    con->cursor += con->width;

    if (con->esc_number_count == 1)
        if (con->esc_num[0]-- > 0)
            cursor_down(con);

    return 1;
}

static int scroll_up(struct console *con) {
    con->current_top = 
        (con->total_size + con->current_top - con->width) % con->total_size;

    if (con->esc_number_count == 1)
        if (con->esc_num[0]-- > 0)
            scroll_up(con);

    return 0;
}

static int scroll_down(struct console *con) {
    con->current_top =
        (con->current_top + con->width) % con->total_size;

    if (con->esc_number_count == 1)
        if (con->esc_num[0]-- > 0)
            scroll_down(con);

    return 0;
}

static int cursor_home(struct console *con) {
    /* 
     * If 2 parameters are provided in the escapce sequence
     * then place the cursor at the given location 
     */
    if (con->esc_number_count == 2) {
        if ((con->esc_num[0] < con->height) && 
                (con->esc_num[1] < con->width))
            con->cursor = con->esc_num[0] * con->width +
                con->esc_num[1];
        else
            return 0;
        /* else simple place it in the upper corner */
    } else 
        con->cursor = 0;

    return 1;
}

static int cursor_end(struct console *con) {
    con->cursor = con->size - 1;

    return 1;
}

static int cursor_save(struct console *con) {
    //  con->cursor_store = con->cursor;
    return 1;
}

static int cursor_restore(struct console *con) {
    //  con->cursor = con->cursor_store;
    return 1;
}

static void erase_data(struct console *con) {
    char *c;
    int cursor  = 0;
    int dir     = 0;

    switch(con->esc_num[0]) {
        case 0: cursor = con->cursor; dir = 1; break;
        case 1: cursor = con->cursor; dir = -1; break;
        case 2: cursor = 0; dir = 1; break;
        default:;
    }

    con->cursor = cursor;

    while ((con->cursor > 0) && (con->cursor < con->size - 1)) {
        con->cursor += dir;
        c = get_cursor_ptr(con);
        *c = '\0';
    }

    con->cursor = cursor;

    return;
}

static int bell(void) {
    return 0;
}

static int backspace(struct console *con) {
    char *c;
    int rc;

    c = get_cursor_ptr(con);

    if ((rc = cursor_back(con)) == 0)
        return 0;

    put_char(con, *c == '\0' ? '\0' : ' ');
    cursor_back(con);
    return 1;
}

enum esc_sequence {
    CHAR = 0,
    ESC_START, 
    ESC_NUMBER,
    ESC_SEMICOLON,
    ESC_LETTER
};

int cwrite(void *conv, char c) {
    struct console *con;
    int rc  = 0;
    int i   = 0;

    con = (struct console *)conv;

    if (con == NULL)
        return -1;

    if (con->buffer == NULL)
        return 0;

    switch (con->state) {
        case CHAR:
            switch (c) {
                case '\n': rc = new_line(con); break;
                case '\t': rc = tab(con); break;
                case '\b': rc = backspace(con); break;
                case '\r': rc = new_line(con); break;
                case '\a': rc = bell(); break;
                case 0x1B : rc = 1; con->state = ESC_START; break;
                default  : rc = put_char(con, c); 
            }
            break;
        case ESC_START:
            if (c == ']') {
                con->esc_number_count = 0;  /* Count of all numbers */
                con->esc_num_digit_i = 0;   /* Count of digits in a number */
                for (i = 0; i < 7; i++)     /* Default numbers in escape seq */
                    con->esc_num[i] = 0;

                con->state = ESC_NUMBER;
                rc = 1;
                break;
            } 
            rc = 0;
            con->state = CHAR;
            break;
        case ESC_NUMBER:
            if ((c >= '0' && c <= '9') && 
                    con->esc_num_digit_i < 3) {
                con->esc_num_tmp[con->esc_num_digit_i++] = c;
                rc = 1;
                break;
            } else if (c == ';') {
                con->esc_num_tmp[con->esc_num_digit_i] = '\0';
                if (con->esc_num_digit_i > 0)
                    con->esc_num[con->esc_number_count++] = atoi(con->esc_num_tmp);
                con->esc_num_digit_i = 0;
                con->state = ESC_SEMICOLON;
                rc = 1;
                break;
            } else {
                con->state = CHAR;
                rc = 0;
                break;
            }
        case ESC_SEMICOLON:
            if ((c >= '0' && c <= '9') && 
                    con->esc_number_count < 8) {  /* At maximum we handle 8 numbers in esc seq */
                con->state = ESC_NUMBER;
                /* Re-evalute this character */
                cwrite(con, c);
                rc = 1;
                break;
            } 
            con->state = ESC_LETTER;
            /* else we fall through */
        case ESC_LETTER:

            switch (c) {
                case 'A': rc = cursor_up(con); break;
                case 'B': rc = cursor_down(con); break;
                case 'C': rc = cursor_forward(con); break;
                case 'D': rc = cursor_back(con); break;
                case 'S': rc = scroll_up(con); break;
                case 'T': rc = scroll_down(con); break;
                case 'J': erase_data(con); break;
                case 'H': 
                case 'j': rc = cursor_home(con); break;
                case 'F': rc = cursor_end(con); break;
                case 's': rc = cursor_save(con); break;
                case 'r': rc = cursor_restore(con); break;
                default: rc = 0; break;
            }
            con->state = CHAR;
        default: break;
    }

    return rc;
}

/* 
 * Print to console 
 */
int cprintf(struct console *con, char *in, ...) {
    va_list args;

    if (con == NULL)
        return -1;

    struct output out = {
        .write = cwrite,
        .data = (void *)con
    };

    va_start(args, in);

    return uprintf(&out, in, args);
}


void console_init(struct console *con, int tab_stop,
        int x, int y, int width, int height,
        int buff_size, char *buff) {
    con->tab_stop = tab_stop > 0 ? tab_stop : 1;
    con->x = x;
    con->y = y;
    con->width = width;
    con->height = height;

    con->size = con->width * con->height;
    con->total_size = buff_size;
    con->buffer = buff;
    /* Current top of the visible part of the buffer */
    con->current_top = 0;   /* Relative with respect to the buffer start */
    con->cursor = 0;        /* Relative with respect to the current top  */
    con->cursor_visible = 1;
    con->cursor_store = 0;

    con->esc_number_count = 0;  /* Count of numbers in escape sequence */
    con->esc_num_digit_i = 0;
    con->visible = 1;

    con->state = CHAR; 

    bzero(buff, buff_size);

    return;
}

void chide(struct console *con) {
    con->visible = 0;
}

void cshow(struct console *con) {
    con->visible = 1;
}

void cdraw(struct console *con) {
    short *screen = (short *)SCREEN_ADDR;
    char *c, co;
    int x, y;

    if (con->visible == 0)
        return;

    c = con->buffer + con->current_top;

    for (y = 0; y < con->height; y++)
        for (x = 0; x < con->width; x++) {
            co = (*c == '\0') ? ' ' : *c;
            screen[(y + con->y) * 80 + (x + con->x)] = 0x07 << 8 | co;
            c++;
            if (c >= con->buffer + con->total_size)
                c = con->buffer;
        }
    /*
       if (con->cursor_visible)
       print_cursor((con->cursor / con->width) + con->y, 
       (con->cursor % con->width) + con->x);
       */
}
