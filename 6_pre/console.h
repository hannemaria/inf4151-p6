#ifndef CONSOLE_H
#define CONSOLE_H

struct console {
    int x;
    int y;
    int width;
    int height;
    int tab_stop;    /* Tabulator stops */

    char *buffer;    /* Console buffer, screen size, and total size */
    int size;
    int total_size;

    int visible;

    int current_top; /* Offset from buffer start */
    int cursor;      /* Offset from current top */

    int cursor_visible;     /* Should console be displayed on screen ? */
    int cursor_store;

    /* Escape sequence specific variables */
    int esc_num_digit_i; /* Position in the following buffer */
    char esc_num_tmp[4]; /* Buffer to store a number from an escape sequence */

    int esc_number_count;
    int esc_num[8]; /* Buffer to store numbers from an escape sequcence */

    int state; /* Current printing state */
};

/*
 * This is set to pre_init or post_init depending on whether
 * initialization is done. A thread in th1.c sets this after
 * scsi subsystem has been initialized
 */
struct console *debug_con;

/* 
 * Used to print debug messages at different phases during
 * boot
 */
struct console *pre_init, *post_init;

void cdraw(struct console *);
void cshow(struct console *);
void chide(struct console *);
int cprintf(struct console *, char *, ...);
int cwrite(void *, char c);
void console_init(struct console *con, int tab_stop,
        int minx, int miny, int maxx, int maxy,
        int size, char *buffer);
#endif 
