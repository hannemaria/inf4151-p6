#include "fs.h"

#ifdef LINUX_SIM
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#endif /* LINUX_SIM */

#include "common.h"
#include "block.h"
#include "util.h"
#include "thread.h"
#include "inode.h"
#include "superblock.h"
#include "kernel.h"
#include "fs_error.h"

#define BITMAP_ENTRIES 256

#define INODE_TABLE_ENTRIES 20

static char inode_bmap[BITMAP_ENTRIES];
static char dblk_bmap[BITMAP_ENTRIES];

static struct mem_inode in_memory_inodes[INODE_TABLE_ENTRIES];
static struct mem_superblock superblock;

static int get_free_entry(char *bitmap);
static int free_bitmap_entry(int entry, char *bitmap);
static inode_t name2inode(const char *name);
static blknum_t ino2blk(inode_t ino);
static blknum_t idx2blk(int index);
int blk2idx(blknum_t blk);

/* help functions */
int check_filedes(pcb_t *p, int fd);
int get_idx_global_inode_table(pcb_t *p, int fd);
int alloc_space(struct mem_inode * inode);
int find_available_in_bitmap(char * bitmap);
void write_superblock_to_secondary_mem();
void write_bitmaps_to_secondary_mem();
int get_idx_global_inode_table();
int get_num_of_blocks_for_inodes();
void clear_in_memory_inodes();
int check_length(char *char_pointer, int value);
int check_filename_exist(inode_t curr_cwd, const char * filename);
int name2inode_in_dir(inode_t curr_cwd, const char * filename);
int get_parent_directory(const char * path);
int get_last_folder(const char * path, char * buf);
int is_inode_open(inode_t inode); /* checks in_memory_inodes if it is open */

/* Adds a new directory entry */
int add_dirent(struct disk_inode * inode, inode_t dirent_inode, const char *dirname);

/* Remove a directory entry */
int remove_dirent(struct disk_inode *d_inode, char *dirname);

/* Gets an inode struct from an inode number */
int get_inode(inode_t inode_num, struct disk_inode * inode);

/* Saves an inode struct to disk on position of inode number */
int save_inode(inode_t inode_num, struct disk_inode * inode);

/* locks */
lock_t open_lock;
/*
 * This function is called by the "loader_thread" after USB
 * subsystem has been initialized.
 *
 * It should check whether there is a filesystem on the
 * disk and perform the necessary operations to prepare it
 * for usage.
 */
void fs_init(void)
{
  block_init();

  /* bzero and set inode numbers to -1 */
  clear_in_memory_inodes();

  block_read_part(0, 0, sizeof(struct disk_superblock), &(superblock.d_super));

  /* Initialize lock */
  lock_init(&open_lock);

  //if (superblock.d_super.ninodes != 0) {
  if (superblock.d_super.initialized == 4) {
    /* createimage writes only zeroes to the reserved area, so if number
       of nodes is zero, it is not initialized   */

    int inodes_sector_size = get_num_of_blocks_for_inodes();

    /* read in inode bitmap */
    block_read_part(inodes_sector_size + 1 /* superblock */, 0,
                    sizeof(char) * BITMAP_ENTRIES, &(inode_bmap[0]));

    /* read in datablock bitmap */
    block_read_part(inodes_sector_size + 1 /* superblock */,
                    sizeof(char) * BITMAP_ENTRIES, sizeof(char) * BITMAP_ENTRIES, &(dblk_bmap[0]));
  } else {
    /* no FS on secondary memory, we have to create one */
    fs_mkfs();
  }
}

/*
 * Make a new file system.
 *
 * The kernel_size is passed to _start(..) in kernel.c by
 * the bootloader.
 */
void fs_mkfs(void)
{
  /* Clear bitmaps */
  bzero(inode_bmap, BITMAP_ENTRIES);
  bzero(dblk_bmap, BITMAP_ENTRIES);

  /* Setup superblock */
  superblock.d_super.ninodes = BITMAP_ENTRIES * 8;
  superblock.d_super.initialized = 4;
  /* Integer division, round up: (dividend + (divisor - 1)) / divisor */
  superblock.d_super.ndata_blks = (512 + 2) - (1 /* superblock */ + superblock.d_super.ninodes + 1 /* bitmaps */);
  superblock.d_super.root_inode = get_free_entry(inode_bmap);
  superblock.d_super.max_filesize = INODE_NDIRECT * BLOCK_SIZE;
  superblock.ibmap = inode_bmap;
  superblock.dbmap = dblk_bmap;
  superblock.dirty = 0;

  /* Add "." and ".." to root dir */
  struct disk_inode root;
  get_inode(superblock.d_super.root_inode, &root);        /* Fetch root inode    */
  root.type = INTYPE_DIR;                                 /* This is a directory */
  add_dirent(&root, superblock.d_super.root_inode, ".");  /* Make "."            */
  add_dirent(&root, superblock.d_super.root_inode, ".."); /* Make ".."           */
  root.size = 2 * sizeof(struct dirent);                  /* Size of two entries */
  save_inode(superblock.d_super.root_inode,  &root);      /* Store to disk       */

  /* Write superblock and bitmaps to secondary memory */
  write_superblock_to_secondary_mem();
  write_bitmaps_to_secondary_mem();
}

int fs_open(const char *filename, int mode)
{

  int fd;
  for (fd = 0; fd < MAX_OPEN_FILES; fd++) {
    if (current_running->filedes[fd].mode == 0) {
      break;
    }
  }

  /* Check that there are more space available */
  if (fd == MAX_OPEN_FILES) {
    return FSE_NOMOREFDTE;
  }

  /* Check correct bitcombination */
  if (/* Has to be read, write or read/write */
      (mode & MODE_RWFLAGS_MASK) == 0 ||
      /* Can not combine MODE_RDONLY with anything */
      ((mode & MODE_RDONLY) && (mode & (MODE_CREAT | MODE_TRUNC | MODE_WRONLY | MODE_RDWR))) ||
      /* Can not combine MODE_WRONLY with read mode */
      ((mode & MODE_WRONLY) && (mode & (MODE_RDWR | MODE_RDONLY)))) {
    return FSE_INVALIDMODE;
  }

  inode_t inode, mem_node;
  struct disk_inode d_inode;
  lock_acquire(&open_lock);


  /* Check if file exist */
  if ((inode = name2inode(filename)) < 0) {
    if ((mode & MODE_CREAT) == 0) {
      return FSE_NOTEXIST; /* File does not exist, and will not be created */
    }

    /* Create new file */
    inode = get_free_entry(inode_bmap);

    /* Load memory from disk */
    if (get_inode(inode, &d_inode) < 0) {
      free_bitmap_entry(inode, inode_bmap);
      return FSE_ERROR;
    }

    /* Initialize */
    d_inode.type = INTYPE_FILE;
    d_inode.size = 0;
    d_inode.nlinks = 0;
    bzero((char*) &(d_inode.direct), sizeof(blknum_t) * INODE_NDIRECT);

    /* Allocate a starter block */
    d_inode.direct[0] = idx2blk(get_free_entry(dblk_bmap));

    /* Store this new file to disk */
    save_inode(inode, &d_inode);

    /* Store an entry of this file in cwd */
    struct disk_inode cwd_inode;
    if (get_inode(current_running->cwd, &cwd_inode) < 0) {
      return FSE_ERROR;
    } else {
      add_dirent(&cwd_inode, inode, filename);      /* Add entry to CWD */
      save_inode(current_running->cwd, &cwd_inode); /* Save CWD to disk */
    }

  } else {
    for (mem_node = 0; mem_node < INODE_TABLE_ENTRIES; mem_node++) {
      if (in_memory_inodes[mem_node].inode_num == inode)
	return FSE_ALREADYOPEN;
    }


    /* File already exist, go get it */
    if (get_inode(inode, &d_inode) < 0) {
      return FSE_ERROR;
    }

    if (d_inode.type == INTYPE_DIR && !(mode & MODE_RDONLY)) {
      /* A folder should never be opened in anything but read only mode */
      return FSE_INVALIDMODE;
    }
  }

  /* Find available spot in memory to store this inode */
  for (mem_node = 0; mem_node < INODE_TABLE_ENTRIES; mem_node++) {
    if (in_memory_inodes[mem_node].inode_num == -1) {
      break;
    }
  }

  if (mem_node == INODE_TABLE_ENTRIES) {
    return FSE_TMOPEN;
  }

  struct mem_inode * memory_inode = &(in_memory_inodes[mem_node]);
  memory_inode->inode_num = inode;
  memory_inode->open_count = 1;
  memory_inode->dirty = 0;
  lock_release(&open_lock);

  bcopy((char *)&d_inode, (char*)&(memory_inode->d_inode), sizeof(struct disk_inode));

  /* Set correct file pointer position */
  if (mode & (MODE_RDONLY | MODE_TRUNC)) {
    memory_inode->pos = 0;
  } else {
    memory_inode->pos = d_inode.size;
  }

  if (mode & MODE_TRUNC) {
    /* Free all datablocks except the first one */
    int num_blocks = (memory_inode->d_inode.size + (DIRENT_BLOCK_SIZE - 1)) / DIRENT_BLOCK_SIZE;

    int i;
    for (i = 1; i < num_blocks; i++) {
      free_bitmap_entry(memory_inode->d_inode.direct[i], dblk_bmap);
    }

    memory_inode->d_inode.size = 0;

    /* Store to disk */
    save_inode(memory_inode->inode_num, &(memory_inode->d_inode));
    write_bitmaps_to_secondary_mem();
  }

  current_running->filedes[fd].mode = mode;
  current_running->filedes[fd].idx = inode;

  return fd;
}

int fs_close(int fd)
{
  /* Get in memory index for this inode */
  int mem_node = get_idx_global_inode_table(current_running, fd);

  if (fd < 0 || fd >= MAX_OPEN_FILES || mem_node < 0 ||
      current_running->filedes[fd].mode == 0) {
    /* not valid or open FD */
    return FSE_INVALIDHANDLE;
  }

  /* save to disk */
  if (in_memory_inodes[mem_node].dirty) {
    save_inode(current_running->filedes[fd].idx, &(in_memory_inodes[mem_node].d_inode));
  }

  /* clear from current running PCB */
  current_running->filedes[fd].idx = 0;
  current_running->filedes[fd].mode = 0;

  /* Clear from memory */
  bzero((char*) &(in_memory_inodes[mem_node]), sizeof(struct mem_inode));
  in_memory_inodes[mem_node].inode_num = -1;

  return FSE_OK;
}

int fs_read(int fd, char *buffer, int size)
{
  if (size < 1) {
    return FSE_OK;
  }

  int curr_fd;
  if ((curr_fd = check_filedes(current_running, fd)) < 0) {
    /* File is not open */
    return FSE_NOPEN;
  }

  if ((current_running->filedes[fd].mode & (MODE_RDONLY | MODE_RDWR)) == 0) {
    /* This process has not the right accesses */
    return FSE_NACCESS;
  }

  int inode_idx;
  if ((inode_idx = get_idx_global_inode_table(current_running, curr_fd)) < 0)
    return inode_idx;

  struct mem_inode * curr_inode = &(in_memory_inodes[inode_idx]);

  /* position in the file */
  int pos = curr_inode->pos;

  /* which datablock 'pos' is pointing on */
  int curr_blk_num = pos / BLOCK_SIZE;

  /* where in the datablock the 'pos' pointer is */
  int offset_in_blk = pos % BLOCK_SIZE;

  int tmp_size = size;
  int offset_buf = 0; /* Offset in the buffer we are use for reading */

  int bytes = size < (BLOCK_SIZE - offset_in_blk) ? size : (BLOCK_SIZE - offset_in_blk);

  while (tmp_size > 0) {
    /* Truncates bytes if we get out of bounds */
    if (curr_inode->pos + bytes > curr_inode->d_inode.size) {
      bytes = curr_inode->d_inode.size - curr_inode->pos;
    }

    curr_inode->pos += bytes;

    block_read_part(curr_inode->d_inode.direct[curr_blk_num], /* block num */
                    offset_in_blk,                           /* offset */
                    bytes,                                   /* data size */
                    buffer+offset_buf);                      /* buffer */

    tmp_size -= bytes;
    offset_buf += bytes;

    bytes = tmp_size >= BLOCK_SIZE ? BLOCK_SIZE : tmp_size;
    curr_blk_num++;
    offset_in_blk = 0;

    if (curr_inode->pos == curr_inode->d_inode.size) {
      break; /* End Of File */
    }
  }

  return offset_buf;
}

int fs_write(int fd, char *buffer, int size)
{
  if (size < 1) {
    return FSE_OK;
  }

  int curr_fd;
  if ((curr_fd = check_filedes(current_running, fd)) < 0) {
    /* File is not open */
    return FSE_NOPEN;
  }

  if ((current_running->filedes[fd].mode & (MODE_WRONLY | MODE_RDWR)) == 0) {
    /* This process has not the right accesses */
    return FSE_NACCESS;
  }

  /* Find the right index to 'in_memory_inodes' */
  int inode_idx;
  if ((inode_idx = get_idx_global_inode_table(current_running, curr_fd)) < 0) {
    return inode_idx;
  }

  struct mem_inode * curr_inode = &(in_memory_inodes[inode_idx]);
  struct disk_inode * d_inode = &(curr_inode->d_inode);

  /* Position in the file */
  int pos = curr_inode->pos;

  /* Which datablock 'pos' is pointing on */
  int curr_blk_num = pos / BLOCK_SIZE;

  /* Where in the datablock the 'pos' pointer is */
  int offset_in_blk = pos % BLOCK_SIZE;

  /* Which datablock the file is ending */
  int eof_blk_num = d_inode->size / BLOCK_SIZE;

  int tmp_size = size;
  int offset_buf = 0; /* Offset in the buffer we are using to read from */
  int bytes = size < (BLOCK_SIZE - offset_in_blk) ? size : (BLOCK_SIZE - offset_in_blk);

  while (tmp_size > 0) {
    /* If current block is bigger than EOF block, need to allocate more space */
    if (curr_blk_num >= eof_blk_num) {
      if (curr_blk_num > eof_blk_num) {
        /* When the buf given spans multiple blocks */
        eof_blk_num++;
      }

      if (curr_blk_num > eof_blk_num || (d_inode->size % BLOCK_SIZE) == 0) {
        if (alloc_space(curr_inode) < 0) { /* Allocate another block */
          save_inode(curr_inode->inode_num, d_inode);
          return (size - tmp_size);
        }
      }
    }

    /* If current block is on the same block as EOF */
    if (curr_blk_num == eof_blk_num){

      /* Expand the filesize if necessary */
      if ((offset_in_blk + bytes) > (d_inode->size % BLOCK_SIZE)){
        d_inode->size += (offset_in_blk + bytes) - (d_inode->size % BLOCK_SIZE);
      }
    }

    block_modify(d_inode->direct[curr_blk_num], /* block num */
                 offset_in_blk,                 /* offset */
                 buffer+offset_buf,             /* buffer */
                 bytes);                        /* data size */

    tmp_size -= bytes;
    offset_buf += bytes;
    curr_inode->pos += bytes;

    bytes = tmp_size >= BLOCK_SIZE ? BLOCK_SIZE : tmp_size;
    curr_blk_num++;
    offset_in_blk = 0;
  }

  /* Save inode on disk */
  save_inode(curr_inode->inode_num, d_inode);

  return size;

}



/*
 * fs_lseek:
 * This function is really incorrectly named, since neither its offset
 * argument or its return value are longs (or off_t's). Also, it will
 * cause blocks to allocated if it extends the file (holes are not
 * supported in this simple filesystem).
 */
int fs_lseek(int fd, int offset, int whence)
{
  int curr_fd, inode_idx, new_pos;

  /* Check current fd is open */
  if ((curr_fd = check_filedes(current_running, fd)) < 0) {
    /* The file is not open */
    return FSE_NOPEN;
  }

  /* Find right index in 'in_memory_inodes'  */
  if ((inode_idx = get_idx_global_inode_table(current_running, curr_fd)) < 0) {
    return inode_idx;
  }

  int max_filesize = superblock.d_super.max_filesize;
  int file_size    = in_memory_inodes[inode_idx].d_inode.size;
  int curr_pos     = in_memory_inodes[inode_idx].pos;

  switch (whence) {
  case SEEK_SET: new_pos =
      offset < max_filesize ? offset : FSE_INVALIDOFFSET; break;

  case SEEK_CUR: new_pos =
      offset + curr_pos < max_filesize ? offset + curr_pos : FSE_INVALIDOFFSET; break;

  case SEEK_END: new_pos =
      offset + file_size < max_filesize ? offset + file_size : FSE_INVALIDOFFSET; break;

  default:
    return FSE_INVALIDWHENCE; /* Not valid whence */
  }

  if (new_pos < 0) {
    /* Either new_pos was to large, or it became a negative offset */
    return FSE_INVALIDOFFSET;
  }

  if (new_pos > file_size) {
    /*
      We have a new position that is larger than current size, then
      we may have to allocate more blocks
    */

    /* Since we have a some padding in inodes used for storing directories: */
    int curr_block_size = in_memory_inodes[inode_idx].d_inode.type == INTYPE_FILE ?
      BLOCK_SIZE : INODE_BLOCK_SIZE;

    int new_offset = new_pos - file_size;
    int num_new_blocks = new_offset / curr_block_size;

    /* Gives us the starting position for the new blocks to be allocated */
    int last_direct = (file_size / curr_block_size) + 1;

    if (last_direct >= INODE_NDIRECT) {
      return FSE_INVALIDOFFSET;
    }

    int i;
    for (i = last_direct; i < num_new_blocks + last_direct; i++) {
      in_memory_inodes[inode_idx].d_inode.direct[i] = get_free_entry(dblk_bmap);
    }

    /* Update new size for inode */
    in_memory_inodes[inode_idx].d_inode.size = new_pos;

    save_inode(in_memory_inodes[inode_idx].inode_num, &(in_memory_inodes[inode_idx].d_inode));
  }

  /* Update new position */
  in_memory_inodes[inode_idx].pos = new_pos;

  return 0;
}

int fs_mkdir(char *dirname)
{

  int error;
  if (dirname == NULL) {
    return FSE_ERROR;      /* dirname is NULL */
  } else if (check_length(dirname, MAX_FILENAME_LEN) < 0) {
    return FSE_NAMETOLONG; /* filename is to long */
  } else if (name2inode(dirname) >= 0) {
    return FSE_EXIST;      /* filename already exists */
  }

  char last_folder[MAX_FILENAME_LEN];
  get_last_folder(dirname, last_folder);

  /* Get inode for current working directory */
  struct disk_inode parent_inode;
  int parent_directory = get_parent_directory(dirname);

  if ((error = get_inode(parent_directory, &parent_inode)) < 0) {
    return error;
  }

  /* Get a free inode and add it to CWD */
  int new_dir_inode_num;
  if ((new_dir_inode_num = get_free_entry(inode_bmap)) < 0) {
    return FSE_NOMOREINODES;
  }

  if ((error = add_dirent(&parent_inode, new_dir_inode_num, last_folder)) < 0) {
    return error;
  }

  /* get a new inode */
  struct disk_inode new_dir_inode;
  if ((error = get_inode(new_dir_inode_num, &new_dir_inode)) < 0) {
    return error;
  }

  /* Clear inode */
  bzero((char*)&new_dir_inode, sizeof(struct disk_inode));

  /* This is a directory */
  new_dir_inode.type = INTYPE_DIR;

  /* Make "." and ".." (fetch new directory and add each dir entry) */
  if ((error = add_dirent(&new_dir_inode, new_dir_inode_num, ".")) < 0) {
    return error;
  }

  if ((error = add_dirent(&new_dir_inode, parent_directory, "..")) < 0) {
    return error;
  }

  new_dir_inode.size = 2 * sizeof(struct dirent);

  /* save cwd-i-node */
  save_inode(parent_directory,  &parent_inode);
  save_inode(new_dir_inode_num,  &new_dir_inode);

  return new_dir_inode_num;
}

int fs_chdir(char *path)
{
  inode_t inode_num;

  /* Check if the current path actually exists */
  if ((inode_num = name2inode(path)) < 0)
    return FSE_NOTEXIST;

  /* Get inode from the corrensponding inode_num */
  struct disk_inode curr_dinode;
  get_inode(inode_num, &curr_dinode);

  /* Type must be a directory */
  if (curr_dinode.type != INTYPE_DIR)
    return FSE_DIRECTORY ;

  current_running->cwd = inode_num;

  return 0;
}

int fs_rmdir(char *path)
{
  inode_t inode_num;

  /* Check if the current path actually exists */
  if ((inode_num = name2inode(path)) < 0)
    return FSE_NOTEXIST;

  /* Get inode from the corrensponding inode_num */
  struct disk_inode curr_dinode;
  get_inode(inode_num, &curr_dinode);

  /* Type must be a directory */
  if (curr_dinode.type != INTYPE_DIR)
    return FSE_DIRECTORY;

  /* Check the directory only contain '.' and '..' */
  if (curr_dinode.size > (2*sizeof(struct dirent)))
    return FSE_DNOTEMPTY;

  /* Mark the corresponds bit in bitmap to available */
  free_bitmap_entry(inode_num, inode_bmap);

  /* Mark the correspond block that contains '.' and '..' to available */
  free_bitmap_entry(blk2idx(curr_dinode.direct[0]), dblk_bmap);

  /* Find cwd inode */
  struct disk_inode parent_inode;
  inode_t parent_inode_num = get_parent_directory(path);
  get_inode(parent_inode_num, &parent_inode);

  /* Remove this path from cwd */
  if (remove_dirent(&parent_inode, path) < 0) {
    return FSE_ERROR;
  }

  /* save inode */
  save_inode(parent_inode_num, &parent_inode);

  return 0;
}

/* Link the new file named 'filename' with 'linkname' */
int fs_link(char *linkname, char *filename)
{
  /* Checks the length of the filename */
  if (check_length(filename, MAX_FILENAME_LEN) < 0)
    return FSE_NAMETOLONG;

  /* Checks if the filename already exits */
  if (check_filename_exist(current_running->cwd, filename) >= 0)
    return FSE_INVALIDNAME;

  /* Find the inode-number to linkname, uses to link filename to linkname */
  inode_t inode_num;
  if ((inode_num = name2inode(linkname)) < 0) {
    return FSE_EXIST;
  }
  struct disk_inode link_inode;
  get_inode(inode_num, &link_inode);

  /* Get the inode to the corrensponding cwd */
  struct disk_inode d_inode;
  get_inode(current_running->cwd, &d_inode);

  /* Must be a file for linking */
  if (link_inode.type == INTYPE_DIR)
    return FSE_LINKDIR;

  /*
   * The filename is unique, therefore create a new dirent-struct in
   * cwd, if enough space.
   */
  int ret;
  if ((ret = add_dirent(&d_inode, inode_num, filename)) < 0) {
    return ret;
  }

  /* Increment the nlinks-value to the corresponding cwd */

  link_inode.nlinks++;

  /* Save CWD (with new dir entry) and linknode to disk (with updated links) */
  save_inode(current_running->cwd, &d_inode);
  save_inode(inode_num, &link_inode);

  return 0;
}

int fs_unlink(char *linkname)
{
  inode_t inode_num;
  if ((inode_num = name2inode(linkname)) < 0) {
    return FSE_NOTEXIST;
  }

  if (is_inode_open(inode_num)) {
    return FSE_OPEN;
  }

  /* Get inode to the corrensponding inode_num */
  struct disk_inode d_inode;
  get_inode(inode_num, &d_inode);

  if (d_inode.type != INTYPE_FILE) {
    /* Can't link directories */
    return FSE_ULINKDIR;
  }

  if (d_inode.nlinks == 0) { /* This is the last link, remove it */
    /* Mark the corresponding bit in bitmap to available */
    free_bitmap_entry(inode_num, inode_bmap);

    /* Mark all blocks occupied of this file as available */
    int num_blocks = (d_inode.size / BLOCK_SIZE) + 1;
    int i;
    for (i = 0; i < num_blocks; i++) {
      free_bitmap_entry(blk2idx(d_inode.direct[i]), dblk_bmap);
    }

  } else {
    /* Decrement the nlinks-value correspond to d_inode */
    d_inode.nlinks--;
  }

  struct disk_inode cwd_inode;
  get_inode(current_running->cwd, &cwd_inode);

  /* Remove directory entry for this link in CWD */
  if (remove_dirent(&cwd_inode, linkname) < 0) {
    return FSE_ERROR;
  }

  /* Since links are updated, need to save to disk */
  save_inode(inode_num, &d_inode);

  /* Since we have removed dir entry, we have to save CWD to disk */
  save_inode(current_running->cwd, &cwd_inode);

  return 0;
}



int fs_stat(int fd, char *buffer)
{
  struct mem_inode curr_inode;
  struct disk_inode d_inode;
  int curr_fd, inode_idx;

  /* Check current fd is open */
  if ((curr_fd = check_filedes(current_running, fd)) < 0) {
    /* The file is not open */
    return FSE_NOPEN;
  }
  /* Find right index in 'in_memory_inodes'  */
  if ((inode_idx = get_idx_global_inode_table(current_running, curr_fd)) < 0) {
    return inode_idx;
  }

  curr_inode = in_memory_inodes[inode_idx];
  d_inode = curr_inode.d_inode;
  bcopy((char *) &d_inode.type, &buffer[0], sizeof(char));
  bcopy((char *) &d_inode.nlinks, &buffer[1], sizeof(short));
  bcopy((char *) &d_inode.size, &buffer[1+sizeof(short)], sizeof(int));

  if (sizeof(char)+sizeof(short)+sizeof(int) != STAT_SIZE) {
    return FSE_ERROR;
  }

  return 0;
}

/*
 * Helper functions for the system calls
 */

/*
 * get_free_entry:
 *
 * Search the given bitmap for the first zero bit.  If an entry is
 * found it is set to one and the entry number is returned.  Returns
 * -1 if all entrys in the bitmap are set.
 */
static int get_free_entry(char *bitmap)
{
  int i;

  /* save status, in case of not altering */
  int temp_dirty = superblock.dirty;
  superblock.dirty = 1;

  /* Seach for a free entry */
  for (i = 0; i < BITMAP_ENTRIES / 8; i++) {
    if (bitmap[i] == 0xff)  /* All taken */
      continue;
    if ((bitmap[i] & 0x80) == 0) {  /* msb */
      bitmap[i] |= 0x80;
      return i * 8;
    } else if ((bitmap[i] & 0x40) == 0) {
      bitmap[i] |= 0x40;
      return i * 8 + 1;
    } else if ((bitmap[i] & 0x20) == 0) {
      bitmap[i] |= 0x20;
      return i * 8 + 2;
    } else if ((bitmap[i] & 0x10) == 0) {
      bitmap[i] |= 0x10;
      return i * 8 + 3;
    } else if ((bitmap[i] & 0x08) == 0) {
      bitmap[i] |= 0x08;
      return i * 8 + 4;
    } else if ((bitmap[i] & 0x04) == 0) {
      bitmap[i] |= 0x04;
      return i * 8 + 5;
    } else if ((bitmap[i] & 0x02) == 0) {
      bitmap[i] |= 0x02;
      return i * 8 + 6;
    } else if ((bitmap[i] & 0x01) == 0) {  /* lsb */
      bitmap[i] |= 0x01;
      return i * 8 + 7;
    }
  }

  /* bitmap is NOT altered, then reset to old value */
  superblock.dirty = temp_dirty;
  return -1;
}

/*
 * free_bitmap_entry:
 *
 * Free a bitmap entry, if the entry is not found -1 is returned, otherwise zero.
 * Note that this function does not check if the bitmap entry was used (freeing
 * an unused entry has no effect).
 */
static int free_bitmap_entry(int entry, char *bitmap)
{
  char *bme;

  if (entry >= BITMAP_ENTRIES)
    return -1;

  bme = &bitmap[entry / 8];

  switch (entry % 8) {
  case 0:
    *bme &= ~0x80;
    break;
  case 1:
    *bme &= ~0x40;
    break;
  case 2:
    *bme &= ~0x20;
    break;
  case 3:
    *bme &= ~0x10;
    break;
  case 4:
    *bme &= ~0x08;
    break;
  case 5:
    *bme &= ~0x04;
    break;
  case 6:
    *bme &= ~0x02;
    break;
  case 7:
    *bme &= ~0x01;
    break;
  }

  /* bitmap is altered, then superblock is dirty */
  superblock.dirty = 1;
  return 0;
}



/*
 * ino2blk:
 * Returns the filesystem block (block number relative to the super
 * block) corresponding to the inode number passed.
 */
static blknum_t ino2blk(inode_t ino)
{
  if (ino < 0 || ino >= superblock.d_super.ninodes) {
    return -1;
  }

  blknum_t superblock = 1;

  int relative_block = (ino * sizeof(struct disk_inode)) / INODE_BLOCK_SIZE;

  return superblock + relative_block;
}

/*
 * idx2blk:
 * Returns the filesystem block (block number relative to the super
 * block) corresponding to the data block index passed.
 */
static blknum_t idx2blk(int index)
{
  if (index < 0) {
    return -1;
  }

  blknum_t superblock = 1;
  blknum_t bitmaps = 1;

  return superblock + get_num_of_blocks_for_inodes() + bitmaps + index;
}

int blk2idx(blknum_t blk) {
  blknum_t superblock = 1;
  blknum_t bitmaps = 1;

  return (((blk - superblock) - get_num_of_blocks_for_inodes()) - bitmaps);
}

/*
 * Returns the parent directory of the final directory in path
 */
int get_parent_directory(const char * path) {
  char temp[MAX_FILENAME_LEN];

  int last_length;
  if ((last_length = get_last_folder(path, temp)) == 0) {
    /* This path is the root */
    return superblock.d_super.root_inode;
  }

  if (path[strlen(path) - 1] == '/') {
    /* Skip trailing backslash */
    last_length++;
  }

  if (strlen(path) - last_length == 0) {
    /* Relative path inside CWD */
    return current_running->cwd;
  }

  strncpy(temp, path, strlen(path) - last_length);
  temp[strlen(path) - last_length] = '\0';

  /* Find i-node for the parent directory */
  return name2inode(temp);
}

/*
 * name2inode:
 * Parses a file name and returns the corresponding inode number. If
 * the file cannot be found, -1 is returned.
 */
static inode_t name2inode(const char *name)
{
  if (strlen(name) == 1 && strncmp(name, "/", 1) == 0) {
    return superblock.d_super.root_inode;
  }

  int filename_pos = 0;

  char cur_dir[MAX_FILENAME_LEN + 1]; /* +1 for zero byte */
  inode_t cur_inode = current_running->cwd;

  if (name[0] == '/') {
    /* Absolute path */
    cur_inode = superblock.d_super.root_inode;
    filename_pos++;
  }

  int not_end = 1, i;
  /* Loop to traverse all elements of a path */
  while (not_end) {

    /* Find next directory or file */
    for (i = 0; i < MAX_FILENAME_LEN; filename_pos++, i++) {
      if (name[filename_pos] == '\0') {
        /* End of path */
        not_end = 0;
        break;
      } else if (name[filename_pos] == '/') {
        /* End of this directory */
        if (name[filename_pos + 1] == '\0') {
          /* ... and also end of path */
          not_end = 0;
        }
        break;
      }

      cur_dir[i] = name[filename_pos];
    }

    if (i == MAX_FILENAME_LEN && name[filename_pos] != '/') {
      /* Filename to long */
      return FSE_NAMETOLONG;
    }

    filename_pos++;
    cur_dir[i] = '\0';

    if ((cur_inode = name2inode_in_dir(cur_inode, cur_dir)) < 0) {
      return -1;
    }
  }

  return cur_inode;
}


int is_inode_open(inode_t inode) {
  int mem_node;
  for (mem_node = 0; mem_node < INODE_TABLE_ENTRIES; mem_node++) {
    if (in_memory_inodes[mem_node].inode_num == inode) {
      return 1;
    }
  }

  return 0;
}

/*
 * Check if the fd exist in filedes and the mode is not 0
 * If it is found, return the right filedescritor index.
 * If not, return -1
 */
int check_filedes(pcb_t *p, int fd) {
  ASSERT2(p != NULL, "The pcb is not supposed to be zero!");

  if (fd > MAX_OPEN_FILES || fd < 0) {
    return -1;
  }

  if ((p->filedes[fd]).mode != 0) {
    /* Exists and is open */
    return fd;
  }

  /* not found */
  return -1;
}

/* Find index in in_memory_inodes from a pcb and fd */
int get_idx_global_inode_table(pcb_t *p, int fd) {
  ASSERT2(p != NULL, "The pcb is not supposed to be zero!");
  int idx = (p->filedes[fd]).idx;

  if (idx < 0) {
    return FSE_INVALIDINDEX;
  }
  int mem_node;
  for (mem_node = 0; mem_node < INODE_TABLE_ENTRIES; mem_node++) {
    if (in_memory_inodes[mem_node].inode_num == idx) {
      break;
    }
  }
  if (mem_node == INODE_TABLE_ENTRIES)
    return FSE_NOPEN;

  /* return the right idx in the global inodetable */
  return mem_node;
}

/*
 * Allocate a new datablock and set the
 * direct pointer.
 */
int alloc_space(struct mem_inode * inode) {

  int idx;

  if ((idx = get_free_entry(dblk_bmap)) < 0) {
    return FSE_FULL; /* No more datablocks */
  }
  int last_used_blk = inode->d_inode.size/BLOCK_SIZE;

  if (last_used_blk+1 >= INODE_NDIRECT) {
    /* not any free direct-pointers */
    return FSE_FULL;
  }

  inode->d_inode.direct[last_used_blk+1] = (blknum_t) idx2blk(idx);

  return inode->d_inode.direct[last_used_blk+1];
}

int get_num_of_blocks_for_inodes() {
  /* Integer rounding up:
     (dividend + (divisor - 1)) / divisor
  */
  return ((superblock.d_super.ninodes * sizeof(struct disk_inode)) - (INODE_BLOCK_SIZE - 1)) / INODE_BLOCK_SIZE;
}

void write_superblock_to_secondary_mem() {
  block_modify(0, 0, &(superblock.d_super), sizeof(struct disk_superblock));

  if (superblock.dirty) {
    write_bitmaps_to_secondary_mem();
    superblock.dirty = 0;
  }
}

void write_bitmaps_to_secondary_mem() {
  int superblock_size = 1;
  int inodes_sector_size = get_num_of_blocks_for_inodes();

  block_modify(inodes_sector_size + superblock_size, 0,
               inode_bmap, BITMAP_ENTRIES);

  block_modify(inodes_sector_size + superblock_size, BITMAP_ENTRIES,
	       dblk_bmap, BITMAP_ENTRIES);
}

void clear_in_memory_inodes() {
  /* Clear in-memory inodes */
  bzero((char*) in_memory_inodes, sizeof(struct mem_inode) * INODE_TABLE_ENTRIES);
  int i;
  for (i = 0; i < INODE_TABLE_ENTRIES; i++) {
    in_memory_inodes[i].inode_num = -1; /* mark as available */
  }
}

/*
 * Adds a directory entry to the inode pointed to by "cwd_inode". Adds the
 * dirent last, and allocates new data blocks if necessary.
 */
int add_dirent(struct disk_inode * cwd_inode, inode_t dirent_inode, const char *dirname) {
  int final_size = cwd_inode->size + sizeof(struct dirent);

  if (final_size > (INODE_NDIRECT * BLOCK_SIZE)) {
    return FSE_FULL;     /* Out of datablocks */
  }

  /* Fill the struct dirent */
  struct dirent d;
  strcpy(d.name, dirname);
  d.name[strlen(dirname)] = '\0';
  d.inode = dirent_inode;

  /* Last direct pointer */
  int direct_pointer = cwd_inode->size / DIRENT_BLOCK_SIZE;

  if (final_size % DIRENT_BLOCK_SIZE == sizeof(struct dirent)) {
    /* we had an overwrap, need to allocate more space */

    if (cwd_inode->size != 0) {
      /* Should not increase direct_pointer for first block */
      direct_pointer++;

      if (direct_pointer >= INODE_NDIRECT) {
        return FSE_ADDDIR;
      }
    }

    int free_entry;
    if ((free_entry = get_free_entry(dblk_bmap)) < 0) {
      return FSE_FULL; /* Out of datablocks */
    }

    /* Store absolute block number */
    cwd_inode->direct[direct_pointer] = idx2blk(free_entry);
  }

  /* Find correct position in block */
  int pos = cwd_inode->size % DIRENT_BLOCK_SIZE;

  /* Update block */
  block_modify(cwd_inode->direct[direct_pointer], pos, &d, sizeof(struct dirent));

  cwd_inode->size = final_size;

  return 0;
}

/*
 * Remove a directory entry to the inode pointed to by "d_inode". Move
 * the dirent last to the same place where removed the directory entry
 * (named "dirname") was placed. If the last dirent is the only dirent
 * that exists in the block before we moved, then we need to set the
 * this block to available after we had moved the last block.
 */
int remove_dirent(struct disk_inode *d_inode, char *dirname) {
  /* In case of a long path, find the last folder */
  char last_folder[MAX_FILENAME_LEN];
  get_last_folder(dirname, last_folder);

  int num_directs, last_direct_pointer, last_block_num;
  int offset_in_last, i;
  struct dirent last_dirent;

  /* Last direct pointer */
  last_direct_pointer = d_inode->size / DIRENT_BLOCK_SIZE;

  /* Last block number used */
  last_block_num = d_inode->direct[last_direct_pointer];

  /* Find the offset in the last dirent */
  offset_in_last = (int) d_inode->size - sizeof(struct dirent);

  /* Read last dirent */
  block_read_part(last_block_num, offset_in_last,
                  sizeof(struct dirent), (char *) &last_dirent);

  /* Integer rounding up:
     (dividend + (divisor - 1)) / divisor
  */
  num_directs = (d_inode->size + (DIRENT_BLOCK_SIZE -1)) / DIRENT_BLOCK_SIZE;

  int bytes_read = 0;

  /* Try to find the current dirname in dirent-struct */
  for (i = 0; i < num_directs; i++) {
    blknum_t block_num = d_inode->direct[i];
    char block[BLOCK_SIZE];

    if (block_read(block_num, block) < 0)
      return -1;

    /* read the block */
    int offset = 0;
    while (offset < DIRENT_BLOCK_SIZE && bytes_read < d_inode->size) {
      struct dirent * curr_dirent = (struct dirent *) (block + offset);



      /* Check if det current name is equal the new filename */
      if (strncmp(last_folder, curr_dirent->name, strlen(last_folder)) == 0) {

        /* If the dirent is not the last dirent we are looking for */
        if ((bytes_read + sizeof(struct dirent)) < d_inode->size)
          block_modify(block_num, bytes_read, &last_dirent, sizeof(struct dirent));

        d_inode->size -= sizeof(struct dirent);

        /* Set the last block to available if it is nothing there */
        if ((d_inode->size + sizeof(struct dirent)) %
            DIRENT_BLOCK_SIZE == sizeof(struct dirent)) {
          free_bitmap_entry(last_block_num , dblk_bmap);
        }
        return 0;
      }
      offset += sizeof(struct dirent);
      bytes_read += sizeof(struct dirent);
    }
  }

  return -1;
}

int get_last_folder(const char * path, char * buf) {
  int max = strlen(path) - 1;
  if (path[max] == '/') {
    /* Skip trailing backslash */
    max--;
  }

  int k = 0;

  /* First calculate length */
  int i;
  for (i = max; i >= 0; i--) {
    if (path[i] != '/') {
      k++;
    } else {
      break;
    }
  }

  int length_of_folder = k;
  buf[k--] = '\0';

  /* Copy last folder name */
  for (i = max; k >= 0; i--, k--) {
    buf[k] = path[i];
  }

  return length_of_folder;
}

/*
 * Reads the inode corresponding to inode_num, and places it in
 * "inode"
 * Returns -1 if couldn't read the correct inode, 0 if correct
 */
int get_inode(inode_t inode_num, struct disk_inode * inode) {
  blknum_t inode_block = ino2blk(inode_num);

  /* Find position in block */
  int block_pos = (inode_num * sizeof(struct disk_inode)) % INODE_BLOCK_SIZE;
  int return_val = block_read_part(inode_block, block_pos, sizeof(struct disk_inode), inode);

  return return_val;
}


/*
 * Saves an inode corresponding to inode_num
 */
int save_inode(inode_t inode_num, struct disk_inode * inode) {
  blknum_t inode_block = ino2blk(inode_num);

  /* Find position in block */
  int block_pos = (inode_num * sizeof(struct disk_inode)) % INODE_BLOCK_SIZE;

  return block_modify(inode_block, block_pos, inode, sizeof(struct disk_inode));
}

/*
 * The char-pointer can not have a length
 * larger then value. Return -1 if it is
 * not smaller
 */
int check_length(char *char_pointer, int value) {
  if (strlen(char_pointer) < value) {
    return 0;
  } else {
    return -1;
  }
}

/*
 *
 */
int check_filename_exist(inode_t curr_cwd, const char * filename) {
  return name2inode_in_dir(curr_cwd, filename);
}

int name2inode_in_dir(inode_t curr_cwd, const char * filename) {
  if (curr_cwd == superblock.d_super.root_inode &&
      strlen(filename) == 1 && strncmp(filename, "/", 1) == 0) {
    return superblock.d_super.root_inode;
  }

  struct disk_inode d_inode;
  int i, num_directs;

  /* Get inode to the corresponding cwd */
  if (get_inode(curr_cwd, &d_inode) < 0)
    return FSE_ERROR;

  /* Integer rounding up:
     (dividend + (divisor - 1)) / divisor
  */
  num_directs = (d_inode.size + (DIRENT_BLOCK_SIZE -1)) / DIRENT_BLOCK_SIZE;

  /* Count of bytes read so far, should not exceed d_inode.size (size of dir) */
  int bytes_read = 0;

  for (i = 0; i < num_directs; i++) {
    blknum_t block_num = d_inode.direct[i];
    char block[BLOCK_SIZE];

    if (block_read(block_num, block) < 0)
      return -1;

    /* read the block */
    int offset = 0;
    while (offset < DIRENT_BLOCK_SIZE && bytes_read < d_inode.size) {
      struct dirent * curr_dirent = (struct dirent *) (block + offset);

      /* Check if det current name is equal the new filename */
      if ((strlen(filename) == strlen(curr_dirent->name)) &&
          strncmp(filename, curr_dirent->name, strlen(filename)) == 0)
        return curr_dirent->inode;

      offset += sizeof(struct dirent);
      bytes_read += sizeof(struct dirent);
    }
  }

  return -1;
}
